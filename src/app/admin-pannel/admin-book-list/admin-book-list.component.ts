import {Component, HostListener, OnInit} from '@angular/core';
import {Book} from "../../entity/Book";
import {BookService} from "../../service/book.service";
import {ProductState} from "../../entity/Content";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {CategoryService} from "../../category-dialog/category-service";
import {Category} from "../../entity/Category";
import {MdDialog} from "@angular/material";
import {Response} from '@angular/http'
import {plainToClass} from "class-transformer";

@Component({
  selector: 'app-admin-book-list',
  templateUrl: './admin-book-list.component.html',
  styleUrls: ['./admin-book-list.component.scss'],
  providers:[BookService,CategoryService]
})
export class AdminBookListComponent implements OnInit {

  books:Book[];
  page :number;
  bookStates:Array<any>;
  showProductState:ProductState = ProductState.Evaluating;
  scrollOffsetToLoad =300;
  categoryFilter=false;
  category:Category;

  constructor(private bookService:BookService,private categoryService:CategoryService,private dialog:MdDialog) {
    this.bookStates = [
      {value: '0', label: 'درحال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'لغو شده'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '4', label: 'آرشیو شده'}
    ];
    this.page = 0;
  }

  ngOnInit() {
    this.loadBooks(this.showProductState);
  }

  changeBookState(event)
  {
    this.page = 0;
    this.scrollOffsetToLoad =300;
    this.showProductState = event.value;
    if(this.categoryFilter){
      // this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    }
    else
      this.loadBooks(event.value);
  }

  loadBooks(pState:ProductState){
     this.bookService.getBooksByState(pState,this.page).subscribe(books =>{
        if(this.page == 0){
          this.books = books;
        }
        else
        {
          for(let book of books)
          {
            this.books.push(book);
          }
        }
     });
  }

  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.page = 0;
      this.scrollOffsetToLoad =300;
      this.category = cat;
      // this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    });
  }

  updateBookState(event){
    console.log(event);
    this.books = this.books.filter(item=>item!=event)
  }

  categorySelection(event)
  {
    this.categoryFilter = event.checked;
    if(this.category!=null&&this.categoryFilter)
    {
      // this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    }
    else
    {
      this.loadBooks(this.showProductState);
    }
  }


  @HostListener('window:scroll', ['$event'])
  loadMore(event) {
    // console.log(window.pageYOffset);
    if(this.scrollOffsetToLoad <= window.pageYOffset)
    {
      this.page = this.page +1 ;
      this.loadBooks(this.showProductState);
      this.scrollOffsetToLoad += 700;
    }
  }
}
