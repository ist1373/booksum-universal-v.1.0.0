import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {BookPlace, PlaceState} from "../../entity/BookPlace";
import {ProgressOfUpload} from "../../main-book/modify-book/modify-book.component";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {CacheService} from "ng2-cache-service";
import {RequestOptions,Headers} from "@angular/http";
import {ProgressHttp} from "angular-progress-http";
import {UploadedFile} from "../../entity/UploadedFile";
import {UniversityService} from "../../service/university.service";
import {University} from "../../entity/University";
import {LocationService} from "../../service/location.service";
import {City} from "../../entity/City";
import {BookPlaceService} from "../../service/book-place.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {User} from "../../entity/User";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-modify-bookplace',
  templateUrl: './modify-bookplace.component.html',
  styleUrls: ['./modify-bookplace.component.scss'],
  providers:[UniversityService,LocationService,BookPlaceService,UserService]
})
export class ModifyBookplaceComponent implements OnInit {


  bookplace:BookPlace;
  placeStates:Array<any>

  bookPlaceForm:FormGroup;

  title = new FormControl('');
  address = new FormControl('');
  description = new FormControl('');
  username = new FormControl('');
  phone = new FormControl('');
  mobile = new FormControl('');
  base = AppContextUrl;
  baseFile = FileContextUrl;
  suggestedUsers :Array<User>;

  cities:Array<any> = [];
  provinces:Array<any> = [];
  universities : Array<any>=[];
  coverImage:File;
  coverImageProgresses = new ProgressOfUpload();


  zoom: number = 5;

  lat = 32.653000;
  lng = 51.668237;

  marker : marker =
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    };


  constructor(private cacheService:CacheService,private http: ProgressHttp,private universityService:UniversityService,
  private locationService:LocationService,private bookPlaceService:BookPlaceService,private snackBar:MdSnackBar,private userService:UserService) {
    this.bookplace = new BookPlace();
    this.initProvinces();
    this.initUniversirsities();


    this.bookPlaceForm = new FormGroup({
      title:this.title,
      address:this.address,
      description:this.description,
      phone:this.phone,
      username:this.username,
      mobile:this.mobile
    });

    // {Evaluating, Activated, Disabled, NeedChange, Rejected}
    this.placeStates =[
      {value: '0', label: 'در حال بررسی'},
      {value: '1', label: 'فعال'},
      {value: '2', label: 'غیر فعال'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '3', label: 'لغو شده'}
    ]
  }

  ngOnInit() {

    this.username.valueChanges.subscribe((val)=>{
      if(val.length > 2)
      {
        this.userService.getUsersByUserName(val).subscribe(users=>{
          console.log(users);
          this.suggestedUsers = users;
        })
      }
      else
      {
        this.suggestedUsers = [];
      }
    });
  }

  selectUser(u:User)
  {
    this.bookplace.createdBy = u;
    this.username.setValue(u.email);
  }


  openFileBrowser(event)
  {
      this.coverImageProgresses = new ProgressOfUpload();
      this.coverImage = event.target.files[0];
  }



  uploadFile(file:File)
  {

    let uploadUrl= AppContextUrl+'file/upload';
    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('accessType', 'Public' );

    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});

    this.http.withUploadProgressListener(progress => {
        this.coverImageProgresses.progress = progress.percentage;
    })
      .post(uploadUrl,formData,options)
      .subscribe((response) => {
        let uploadedFile = new UploadedFile();
        uploadedFile.id = response.json().code;
          this.bookplace.coverImage = uploadedFile;
          this.coverImageProgresses.complete = true;
      })
  }


  initUniversirsities()
  {
    this.universityService.getUniversities().subscribe((unis:University[])=>{
      for(let uni of unis)
      {
        this.universities.push({value:uni.id,label:uni.title.toString()});
      }
    });
    this.cacheService.set('unis',this.universities);
  }

  public selectUniversity(item:any):void {
    console.log('Selected value is: ', item.value);
    let uni = new University();
    uni.id = item.value;
    this.bookplace.university = uni;
  }


  initProvinces(){
    this.locationService.getProvinces().subscribe(provs =>{
      for(let prov of provs)
      {
        this.provinces.push({value:prov.id,label:prov.title.toString()})
      }
    })
  }
  public selectProvince(item:any):void {
    console.log('Selected value is: ', item.value);
    this.cities = [];
    this.getCitis(item.value);
  }

  selectPlaceState(item:any){
    this.bookplace.placeState = item.value;
  }

  getCitis(provinceId:number){

    this.locationService.getCities(provinceId).subscribe(cities =>{
      console.log(cities);
      for(let city of cities)
      {
        this.cities.push({value:city.id,label:city.title.toString()})
      }
    });
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: any) {
    this.marker = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      label: this.title.value,
      draggable: false
    };
    this.bookplace.latitude = $event.coords.lat;
    this.bookplace.longitude = $event.coords.lng;
  }


  public selectCity(item:any):void {
    console.log('Selected value is: ', item.value);
    let city = new City();
    city.id = item.value;
    this.bookplace.city = city;
  }


  saveBookPlace()
  {
    this.bookplace.mobileNumber = this.mobile.value;
    this.bookplace.title = this.title.value;
    this.bookplace.address = this.address.value;
    this.bookplace.description = this.description.value;
    this.bookplace.phoneNumber = this.phone.value;
    console.log(this.bookplace);
    this.bookPlaceService.addBookPlace(this.bookplace).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
      {
        this.openSnackBar('ذخیره مکان کتاب با موفقیت انجام شد','success');
        this.clearForms();
      }
      else
        this.openSnackBar(res.json()['message'],'error');
    });
  }

  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }

  clearForms(){
    this.mobile.setValue('');
    this.title.setValue('');
    this.address.setValue('');
    this.phone.setValue('');
    this.description.setValue('');
    this.coverImage = null;

    this.coverImageProgresses = new ProgressOfUpload();
    this.bookplace = new BookPlace();
  }

}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
