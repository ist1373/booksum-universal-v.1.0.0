import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BookPlace, PlaceState} from '../../entity/BookPlace';
import {ProgressOfUpload} from '../../main-book/modify-book/modify-book.component';
import {AppContextUrl, FileContextUrl} from '../../globals';
import {CacheService} from 'ng2-cache-service';
import {RequestOptions,Headers} from '@angular/http';
import {ProgressHttp} from 'angular-progress-http';
import {UploadedFile} from '../../entity/UploadedFile';
import {UniversityService} from '../../service/university.service';
import {University} from '../../entity/University';
import {LocationService} from '../../service/location.service';
import {City} from '../../entity/City';
import {BookPlaceService} from '../../service/book-place.service';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Store} from '../../entity/Store';
import {StoreService} from '../../service/store.service';
import {User} from "../../entity/User";
import {UserService} from "../../service/user.service";
import {Marker,LatLng} from "@agm/core/services/google-maps-types";
import {number} from "ng2-validation/dist/number";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-modify-store',
  templateUrl: './modify-store.component.html',
  styleUrls: ['./modify-store.component.scss'],
  providers:[UniversityService,LocationService,BookPlaceService,StoreService,UserService]
})
export class ModifyStoreComponent implements OnInit {

  modificationType:ModificationType;
  ModificationType:typeof ModificationType = ModificationType;
  store:Store;
  storeStates:Array<any>
  base = AppContextUrl;
  baseFile = FileContextUrl;
  storeForm:FormGroup;

  title = new FormControl('');
  address = new FormControl('');
  description = new FormControl('');
  username = new FormControl('');
  phone = new FormControl('');
  mobile = new FormControl('');

  // cities:Array<any> = [];
  // provinces:Array<any> = [];
  // universities : Array<any>=[];
  suggestedUsers : Array<any>=[];
  coverImage:File;
  coverImageProgresses = new ProgressOfUpload();


  // google maps zoom level
  zoom: number = 5;

  lat = 32.653000;
  lng = 51.668237;

  marker : marker =
  {
    lat: 51.673858,
    lng: 7.815982,
    label: 'A',
    draggable: true
  };



  constructor(private activatedRoute:ActivatedRoute,private cacheService:CacheService,private http: ProgressHttp,private universityService:UniversityService,
              private locationService:LocationService,private storeService:StoreService,private snackBar:MdSnackBar,private userService:UserService) {
    this.store = new Store();
    // this.initProvinces();
    // this.initUniversirsities();


    this.storeForm = new FormGroup({
      title:this.title,
      address:this.address,
      description:this.description,
      phone:this.phone,
      username:this.username,
      mobile:this.mobile
    });

    // {Evaluating, Activated, Disabled, NeedChange, Rejected}
    this.storeStates =[
      {value: '0', label: 'در حال بررسی'},
      {value: '1', label: 'فعال'},
      {value: '2', label: 'غیر فعال'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '3', label: 'لغو شده'}
    ];

    this.activatedRoute.url.subscribe((url)=>{
      if(url[2].toString()=='edit')
      {
         this.modificationType = ModificationType.Edit
         this.storeService.getStoreByID(Number(url[3])).subscribe((store:Store) =>{
           this.store = store;
           console.log(store);
           this.title.setValue(store.title);
           this.address.setValue(store.address);
           this.mobile.setValue(store.mobileNumber);
           this.phone.setValue(store.phoneNumber);


         });
      }
      else
      {
        this.modificationType = ModificationType.New;
      }
    });

  }

  ngOnInit() {

    this.username.valueChanges.subscribe((val)=>{
      if(val.length > 2)
      {
        this.userService.getUsersByUserName(val).subscribe(users=>{
          console.log(users);
          this.suggestedUsers = users;
        })
      }
      else
      {
        this.suggestedUsers = [];
      }
    });
  }



  openFileBrowser(event)
  {
    this.coverImageProgresses = new ProgressOfUpload();
    this.coverImage = event.target.files[0];
  }



  uploadFile(file:File)
  {

    let uploadUrl= AppContextUrl+'file/upload';
    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('accessType', 'Public' );

    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});

    this.http.withUploadProgressListener(progress => {
      this.coverImageProgresses.progress = progress.percentage;
    })
      .post(uploadUrl,formData,options)
      .subscribe((response) => {
        let uploadedFile = new UploadedFile();
        uploadedFile.id = response.json().code;
        this.store.coverImage = uploadedFile;
        this.coverImageProgresses.complete = true;
      })
  }


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: any) {
    this.marker = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      label: this.title.value,
      draggable: false
    };
    this.store.latitude = $event.coords.lat;
    this.store.longitude = $event.coords.lng;
  }

  // initUniversirsities()
  // {
  //   this.universityService.getUniversities().subscribe((unis:University[])=>{
  //     for(let uni of unis)
  //     {
  //       this.universities.push({value:uni.id,label:uni.title.toString()});
  //     }
  //   });
  //   this.cacheService.set('unis',this.universities);
  // }

  // public selectUniversity(item:any):void {
  //   console.log('Selected value is: ', item.value);
  //   let uni = new University();
  //   uni.id = item.value;
  //   this.store.university = uni;
  // }


  // initProvinces(){
  //   this.locationService.getProvinces().subscribe(provs =>{
  //     for(let prov of provs)
  //     {
  //       this.provinces.push({value:prov.id,label:prov.title.toString()})
  //     }
  //   })
  // }
  // public selectProvince(item:any):void {
  //   console.log('Selected value is: ', item.value);
  //   this.cities = [];
  //   this.getCitis(item.value);
  // }

  selectStoreState(item:any){
    this.store.storeState = item.value;
  }

  // getCitis(provinceId:number){
  //
  //   this.locationService.getCities(provinceId).subscribe(cities =>{
  //     console.log(cities);
  //     for(let city of cities)
  //     {
  //       this.cities.push({value:city.id,label:city.title.toString()})
  //     }
  //   });
  // }

  // public selectCity(item:any):void {
  //   console.log('Selected value is: ', item.value);
  //   let city = new City();
  //   city.id = item.value;
  //   this.store.cities.push(city) ;
  // }

  selectUser(u:User)
  {
    this.store.createdBy = u;
    this.username.setValue(u.email);
  }

  saveStore()
  {
    this.store.title = this.title.value;
    this.store.address = this.address.value;
    this.store.description = this.description.value;
    this.store.phoneNumber = this.phone.value;
    this.store.mobileNumber = this.mobile.value;
    console.log(this.store);
    this.storeService.addStore(this.store).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
      {
        this.openSnackBar('ذخیره فروشگاه با موفقیت انجام شد','success');
        this.clearForms();
      }
      else
        this.openSnackBar(res.json()['message'],'error');
    });
  }

  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }

  clearForms(){
    this.mobile.setValue('');
    this.title.setValue('');
    this.address.setValue('');
    this.phone.setValue('');
    this.description.setValue('');
    this.coverImage = null;
    this.username.setValue('');
    this.coverImageProgresses = new ProgressOfUpload();
    this.store = new Store();
  }

}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
export enum ModificationType{Edit,New};
