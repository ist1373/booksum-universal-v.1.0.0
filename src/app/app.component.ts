import {Component, Inject, PLATFORM_ID} from '@angular/core';
import {BooksumCacheService} from './service/booksum-cache.service';
import {CacheService} from 'ng2-cache-service';
import {UserService} from './service/user.service';
import {UniversityService} from './service/university.service';
import {HttpInterceptorService} from 'ng-http-interceptor';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import {TourService} from 'ngx-tour-core';
import {User} from "./entity/User";
import {MdDialog, MdDialogConfig, MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {LoginRegisterComponent} from "./login-register/login-register.component";
import {Router} from "@angular/router";
import {Ng2DeviceService} from "ng2-device-detector";
import {ExpireTime} from "./globals";
// import {MetaService} from "@ngx-meta/core";
// import {CacheStoragesEnum} from "ng2-cache-service/dist/src/enums/cache-storages.enum";

// declare var introJs: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [BooksumCacheService, CacheService, UserService, UniversityService]
})
export class AppComponent {
  title = 'app works!';
  user:User;
  // intro = introJs();
  isAndroidOS = false;
  // static intro2 = introJs();

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
              private cacheService: CacheService, httpInterceptor: HttpInterceptorService,
              private slimLoadingBarService: SlimLoadingBarService,public dialog:MdDialog,private router: Router,
              private deviceService: Ng2DeviceService,public snackBar: MdSnackBar) {
    // this.ua = window.navigator.userAgent




    // this.cacheService.useStorage(CacheStoragesEnum.LOCAL_STORAGE);
    // init();

    // this.meta.setTitle('imannnnnnnnnnnnnnnn', true);
    // this.meta.setTag('og:image', 'http://localhost:4200/assets/images/internet-download-manager.png');


    this.user = this.cacheService.get('user');
    if (isPlatformBrowser(this.platformId)) {
      // Client only code.
    }
    if (isPlatformServer(this.platformId)) {
      // Server only code.
    }


    httpInterceptor.request().addInterceptor((data, method) => {
      // console.log(method, data);
      slimLoadingBarService.start();

      return data;

    });

    httpInterceptor.response().addInterceptor((res, method) => {
      return res.do(r => {
        slimLoadingBarService.complete();
      });
    });
  }
  ngOnInit() {

    if(this.deviceService.os == 'android')
    {

      if(!this.cacheService.get('hideAndroidApp'))
      {
        this.isAndroidOS = true;
      }
    }
    // setTimeout(res=>{
    //   this.intro.setOptions({
    //     steps: [
    //       {
    //         element: '#step_one_element_id',
    //         intro: "Step one description",
    //         position: 'right'
    //       },
    //       {
    //         element: '#step_two_element_id',
    //         intro: "Step <i>two</i> description",
    //         position: 'bottom'
    //       },
    //       {
    //         element: '#step_three_element_id',
    //         intro: 'Step <span style="color: green;">three</span> description',
    //         position: 'left'
    //       }
    //     ]
    //   });
      // this.intro.start();
    // },5000)


  }
  addBook(){
    if(this.user!=null)
    {
      this.router.navigate(['user','book','new']);
    }
    else
    {
      console.log('ffffffffffffff');
      this.openLoginRegisterModal(true);
    }
  }

  openLoginRegisterModal(isSignIn: boolean)
  {

    let config = new MdDialogConfig();
    config.width = '400px';

    const dialogRef = this.dialog.open(LoginRegisterComponent);
    dialogRef.componentInstance.signInTap = isSignIn;

  }

  openAndroidAppSnackBar() {
    // let conf = new MdSnackBarConfig();
    //
    //
    // conf.duration = 5000;
    // conf.extraClasses = ['success'];
    // this.snackBar.open('ثبت نام شما با موفقیت انجام شد', '', conf);
  }
  closeAndroidBottomBar(){
    this.isAndroidOS = false;
    this.cacheService.set('hideAndroidApp',true,{expires: Date.now() + ExpireTime});
  }
}
