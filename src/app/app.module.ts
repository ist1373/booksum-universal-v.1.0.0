// import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent} from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { BookComponent } from './main-book/book/book.component';
import { BookDetailsComponent } from './main-book/book-details/book-details.component';
import { CountryMapComponent } from './country-map/country-map.component';
import { BookCarouselComponent } from './main-book/book-carousel/book-carousel.component';
import { MainBookComponent } from './main-book/main-book.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdCheckboxModule,
  MdChipsModule,
  MdDialogModule, MdProgressBarModule, MdSlideToggleModule, MdSnackBarModule, MdTabsModule,
  MdTooltipModule
} from '@angular/material';
import { StarRatingModule } from 'angular-star-rating';
import { TermComponent } from './term/term.component';
import {CategoryDialogComponent} from './category-dialog/category-dialog.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import {CacheService, LocalStorageServiceModule} from 'ng2-cache-service';
import {ProgressHttpModule} from 'angular-progress-http';
import {GalleryConfig, GalleryModule} from 'ng-gallery';

import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {HttpInterceptorModule} from 'ng-http-interceptor';
import {BrowserModule} from '@angular/platform-browser';
import {CacheStorageAbstract} from 'ng2-cache-service/dist/src/services/storage/cache-storage-abstract.service';
import {CacheLocalStorage} from 'ng2-cache-service/dist/src/services/storage/local-storage/cache-local-storage.service';
import {appRoutes} from './app.routes';
import {AuthGuard} from './guards/auth.guard';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { AdminPannelComponent } from './admin-pannel/admin-pannel.component';
import { AdminHeaderComponent } from './admin-pannel/admin-header/admin-header.component';
import {SelectModule} from 'ng-select';
import {CommonModule} from '@angular/common';
import { UserHeaderComponent } from './user-panel/user-header/user-header.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UserDetailsComponent } from './user-panel/user-details/user-details.component';

import { SitemapComponent } from './sitemap/sitemap.component';
import { UserFinancialHeaderComponent } from './user-panel/user-financials/user-financial-header/user-financial-header.component';
import { UserFinancialAccountComponent } from './user-panel/user-financials/user-financial-account/user-financial-account.component';
import { UserTransactionsComponent } from './user-panel/user-financials/user-transactions/user-transactions.component';
import { UserPaymentsComponent } from './user-panel/user-financials/user-payments/user-payments.component';
import { UserPonyComponent } from './user-panel/user-financials/user-pony/user-pony.component';
import { ModifyBookComponent } from './main-book/modify-book/modify-book.component';

import { SpecialBookComponent } from './main-book/special-book/special-book.component';
import {TagInputModule} from 'ngx-chips';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { CategorySectionComponent } from './main-book/category-section/category-section.component';
import { BookListComponent } from './main-book/book-list/book-list.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { BookPlaceDialogComponent } from './main-book/book-details/book-place-dialog/book-place-dialog.component';
import { ModifyBookplaceComponent } from './admin-pannel/modify-bookplace/modify-bookplace.component';
import {FileSizePipe} from "./utils/FileSizePipe";
import { ModifyStoreComponent } from './admin-pannel/modify-store/modify-store.component';
import { StoreDialogComponent } from './main-book/book-details/store-dialog/store-dialog.component';
import {AgmCoreModule} from "@agm/core";
import { SellerHeaderComponent } from './seller-panel/seller-header/seller-header.component';
import { SellerBookListComponent } from './seller-panel/seller-book-list/seller-book-list.component';
import { SellerStoreListComponent } from './seller-panel/seller-store-list/seller-store-list.component';
import { BrokerHeaderComponent } from './broker-pannel/broker-header/broker-header.component';
import { BrokerBookListComponent } from './broker-pannel/broker-book-list/broker-book-list.component';
import { BrokerAddBookComponent } from './broker-pannel/broker-add-book/broker-add-book.component';
import { BrokerSelectDialogComponent } from './broker-pannel/broker-add-book/broker-select-dialog/broker-select-dialog.component';
import { BrokerRequestListComponent } from './broker-pannel/broker-request-list/broker-request-list.component';
import { JalaliPipePipe } from './utils/jalali-pipe.pipe';
import { DecodeUrlPipe } from './utils/decode-url.pipe';
import {LazyLoadImageModule} from "ng-lazyload-image";
import {Ng2DeviceDetectorModule} from "ng2-device-detector";
import {MetaModule} from "@ngx-meta/core";
import {UserBookListComponent} from "./user-panel/user-book-list/user-book-list.component";
import {AdminBookListComponent} from "./admin-pannel/admin-book-list/admin-book-list.component";
import {OwlModule} from "ngx-owl-carousel";
import {PersainNumberPipe} from "./utils/persain-number.pipe";
import {CityDialogComponent} from "./city-dialog/city-dialog.component";
import {TourMdMenuModule} from "ngx-tour-md-menu";
import {RegisterationConfirmComponent} from "./login-register/registeration-confirm/registeration-confirm.component";
import {StorePageComponent} from "./stores/store-page/store-page.component";
import {StoreListComponent} from "./stores/store-list/store-list.component";





export const galleryConfig : GalleryConfig = {
  "style": {
    "background": "#121519",
    "width": "100%",
    "height": "100%",
    "padding": "1em"
  },
  "animation": "fade",
  "loader": {
    "width": "50px",
    "height": "50px",
    "position": "center",
    "icon": "oval"
  },
  "description": {
    "position": "top",
    "overlay": false,
    "text": false,
    "counter": true
  },
  "bullets": {
    "position": "bottom"
  },
  "player": {
    "autoplay": false,
    "speed": 3000
  },
  "thumbnails": {
    "width": 120,
    "height": 90,
    "position": "bottom",
    "space": 20
  }
}



@NgModule({
  schemas:[NO_ERRORS_SCHEMA],
  declarations: [
    FileSizePipe,
    AppComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    BookComponent,
    BookDetailsComponent,
    CountryMapComponent,
    BookCarouselComponent,
    MainBookComponent,
    TermComponent,
    CategoryDialogComponent,
    LoginRegisterComponent,
    AccessDeniedComponent,
    AdminPannelComponent,
    AdminHeaderComponent,
    UserHeaderComponent,
    ContactUsComponent,
    UserDetailsComponent,
    SitemapComponent,
    UserFinancialHeaderComponent,
    UserFinancialAccountComponent,
    UserTransactionsComponent,
    UserPaymentsComponent,
    UserPonyComponent,
    ModifyBookComponent,
    SpecialBookComponent,
    CategorySectionComponent,
    BookListComponent,
    AboutUsComponent,
    BookPlaceDialogComponent,
    ModifyBookplaceComponent,
    ModifyStoreComponent,
    StoreDialogComponent,
    SellerHeaderComponent,
    SellerBookListComponent,
    SellerStoreListComponent,
    BrokerHeaderComponent,
    BrokerBookListComponent,
    BrokerAddBookComponent,
    UserBookListComponent,
    StoreListComponent,
    AdminBookListComponent,
    RegisterationConfirmComponent,
    CityDialogComponent,
    BrokerSelectDialogComponent,
    BrokerRequestListComponent,
    StorePageComponent,
    JalaliPipePipe,
    DecodeUrlPipe,
    PersainNumberPipe
  ],
  entryComponents: [CityDialogComponent,CategoryDialogComponent, LoginRegisterComponent,BookPlaceDialogComponent,StoreDialogComponent,BrokerSelectDialogComponent],
  imports: [
    Ng2DeviceDetectorModule.forRoot(),
    BrowserModule.withServerTransition({appId: 'booksum-universal'}),
    TagInputModule,
    HttpInterceptorModule,
    MdChipsModule,
    FormsModule,
    ReactiveFormsModule,
    MdSnackBarModule,
    HttpModule,
    CommonModule,
    BrowserAnimationsModule,
    MdTooltipModule,
    MdTabsModule,
    MdDialogModule,
    StarRatingModule,
    ProgressHttpModule,
    SelectModule,
    MdSlideToggleModule,
    MdCheckboxModule,
    MdProgressBarModule,
    LocalStorageServiceModule,
    LazyLoadImageModule,
    OwlModule,
    MetaModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    GalleryModule.forRoot(galleryConfig),
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAllgFjAGzFInZysR3Icp4ZSQwGsJ2RCeI'
    })
  ],
  providers: [{
    provide: CacheStorageAbstract,
    useClass: CacheLocalStorage},
    AuthGuard,
    CacheService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

