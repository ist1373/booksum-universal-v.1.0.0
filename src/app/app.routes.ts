import {Routes} from '@angular/router';
import {CountryMapComponent} from './country-map/country-map.component';
import {MainBookComponent} from './main-book/main-book.component';
import {BookDetailsComponent} from './main-book/book-details/book-details.component';
import {TermComponent} from './term/term.component';
import {AccessDeniedComponent} from './access-denied/access-denied.component';
import {AdminPannelComponent} from './admin-pannel/admin-pannel.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {UserDetailsComponent} from './user-panel/user-details/user-details.component';
import {SitemapComponent} from './sitemap/sitemap.component';
import {UserFinancialAccountComponent} from './user-panel/user-financials/user-financial-account/user-financial-account.component';
import {UserTransactionsComponent} from './user-panel/user-financials/user-transactions/user-transactions.component';
import {UserPaymentsComponent} from './user-panel/user-financials/user-payments/user-payments.component';
import {UserPonyComponent} from './user-panel/user-financials/user-pony/user-pony.component';
import {ModifyBookComponent} from "./main-book/modify-book/modify-book.component";
import {AboutUsComponent} from "./about-us/about-us.component";
import {BookListComponent} from "./main-book/book-list/book-list.component";
import {ModifyBookplaceComponent} from "./admin-pannel/modify-bookplace/modify-bookplace.component";
import {ModifyStoreComponent} from "./admin-pannel/modify-store/modify-store.component";
import {SellerBookListComponent} from "./seller-panel/seller-book-list/seller-book-list.component";
import {SellerStoreListComponent} from "./seller-panel/seller-store-list/seller-store-list.component";
import {BrokerBookListComponent} from "./broker-pannel/broker-book-list/broker-book-list.component";
import {BrokerAddBookComponent} from "./broker-pannel/broker-add-book/broker-add-book.component";
import {BrokerRequestListComponent} from "./broker-pannel/broker-request-list/broker-request-list.component";
import {UserBookListComponent} from "./user-panel/user-book-list/user-book-list.component";
import {AdminBookListComponent} from "./admin-pannel/admin-book-list/admin-book-list.component";
import {RegisterationConfirmComponent} from "./login-register/registeration-confirm/registeration-confirm.component";
import {StorePageComponent} from "./stores/store-page/store-page.component";
import {StoreListComponent} from "./stores/store-list/store-list.component";
/**
 * Created by iman on 6/13/2017.
 */

export const appRoutes: Routes = [
  // { path: '', redirectTo: 'book/home' , pathMatch: 'full'},


  { path: '', component: MainBookComponent,pathMatch: 'full' },
  { path: 'toranj', component: StorePageComponent },

  { path: 'book/list/:group', component: BookListComponent },
  { path: 'book/cat/:id', component: BookListComponent },
  { path: 'book/cat/:id/:name', component: BookListComponent },
  { path: 'book/tag/:id', component: BookListComponent },
  { path: 'book/search', component: BookListComponent },
  { path: 'book/:uid/:name', component: BookDetailsComponent },
  { path: 'book/:uid', component: BookDetailsComponent },


  { path: 'store/:id', component: StorePageComponent },
  { path: 'stores', component: StoreListComponent},




  { path: 'admin', component: AdminPannelComponent},
  // { path: 'admin/content/new', component: ModifyBookletComponent},
  // { path: 'admin/content/edit/:id', component: ModifyBookletComponent },
  // { path: 'admin/comments', component: CommentsListComponent},
  // { path: 'admin/contents', component: AdminBookletListComponent },
  { path: 'admin/book/new', component: ModifyBookComponent},

  { path: 'admin/book/edit/:id', component: ModifyBookComponent},
  { path: 'admin/book/list', component: AdminBookListComponent},
  { path: 'admin/bookplace/new', component: ModifyBookplaceComponent},
  { path: 'admin/store/new', component: ModifyStoreComponent},
  // canActivate: [AuthGuard]


  { path: 'user/details', component: UserDetailsComponent},
  { path: 'user/financial/account', component: UserFinancialAccountComponent},
  { path: 'user/financial/transactions', component: UserTransactionsComponent},
  { path: 'user/financial/payments', component: UserPaymentsComponent},
  { path: 'user/financial/pony', component: UserPonyComponent},
  { path: 'user/book/new', component: ModifyBookComponent},
  { path: 'user/book/list', component: UserBookListComponent},
  { path: 'user/registrationConfirm/:token', component: RegisterationConfirmComponent},



  { path: 'seller/book/new', component: ModifyBookComponent},
  { path: 'seller/book/edit/:id', component: ModifyBookComponent},
  { path: 'seller/book/list', component: SellerBookListComponent},
  { path: 'seller/store/list', component: SellerStoreListComponent},
  { path: 'seller/store/edit/:id', component: ModifyStoreComponent},



  { path: 'broker/book/add', component: BrokerAddBookComponent},
  { path: 'broker/book/list', component: BrokerBookListComponent},
  { path: 'broker/book/requestes', component: BrokerRequestListComponent},

  { path: 'sitemap', component: SitemapComponent},

  { path: 'selectcity', component: CountryMapComponent },
  { path: 'terms', component: TermComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'AccessDenied', component: AccessDeniedComponent }
  // { path: 'home/:city', component: MainPageComponent,children:ChildRoutes}

];
