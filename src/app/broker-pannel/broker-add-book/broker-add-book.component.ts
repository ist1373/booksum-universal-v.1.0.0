import { Component, OnInit } from '@angular/core';
import {BookService} from "../../service/book.service";
import {Book} from "../../entity/Book";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-broker-add-book',
  templateUrl: './broker-add-book.component.html',
  styleUrls: ['./broker-add-book.component.scss'],
  providers:[BookService]
})
export class BrokerAddBookComponent implements OnInit {

  page = 0;
  books : Array<Book>;
  searchForm:FormGroup;
  title = new FormControl('');

  constructor(private bookService:BookService) { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      title:this.title
    });

    this.title.valueChanges.subscribe((val)=>{
      if(val.length>=2)
      {
        this.page = 0;
        this.loadBooks(val);
      }
    });
  }



  loadBooks(title:string)
  {
      this.bookService.getStorableBookByTitle(title,this.page).subscribe(books=>{
        console.log(books);
        if(this.page == 0)
          this.books = books;
        else
        {
          for(let book of books)
          {
            this.books.push(book);
          }
        }
      });
  }
}
