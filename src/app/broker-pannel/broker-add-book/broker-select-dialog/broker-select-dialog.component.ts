import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../entity/User";
import {CacheService} from "ng2-cache-service";
import {BookPlace} from "../../../entity/BookPlace";
import {BookPlaceService} from "../../../service/book-place.service";
import {Book} from "../../../entity/Book";
import {BookService} from "../../../service/book.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";

@Component({
  selector: 'app-broker-select-dialog',
  templateUrl: './broker-select-dialog.component.html',
  styleUrls: ['./broker-select-dialog.component.scss'],
  providers:[CacheService,BookPlaceService,BookService]
})
export class BrokerSelectDialogComponent implements OnInit {


  book:Book;
  user:User = this.cacheService.get('user');
  constructor(private snackBar:MdSnackBar,private cacheService:CacheService,private bookPlaceService:BookPlaceService,private bookService:BookService) { }

  bookplaceItems:Array<BookplaceItem> = [];


  ngOnInit() {
    this.bookPlaceService.getBookPlaceByUser(this.user).subscribe(bookplaces =>{
      for(let bp of bookplaces)
      {
        let item= new BookplaceItem();
        item.bookplace = bp;
        if(this.existOnBookplace(bp.id))
        {
          item.checked = true;
        }
        this.bookplaceItems.push(item);
      }

    });
  }
  existOnBookplace(id:number):boolean
  {
    let res = false;
    for(let bp of this.book.bookPlaces)
    {
      if(bp.id == id)
        res = true;
    }
    return res;
  }

  addBook(){
    for(let bpi of this.bookplaceItems)
    {
      if(bpi.checked)
      {
        this.bookService.addBookToBookPlace(this.book.id,bpi.bookplace.id).subscribe(res=>{
          console.log(res);
            this.openSnackBar('عملیات با موفقیت انجام شد','success');

        });
      }
      else
      {
        this.bookService.deleteBookFromBookPlace(this.book.id,bpi.bookplace.id).subscribe(res=>{
          console.log(res);
        });
      }
    }
  }
//   if(res.json()['success']==true)
//   this.openSnackBar('رزرو کتاب با موفقیت انجام شد','success');
//   else
//   this.openSnackBar(res.json()['message'],'error');
// });

  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }

}
export class BookplaceItem{
  bookplace:BookPlace;
  checked : boolean=  false;
}
