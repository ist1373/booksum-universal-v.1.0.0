import { Component, OnInit } from '@angular/core';
import {Book} from "../../entity/Book";
import {BookService} from "../../service/book.service";
import {BookPlaceService} from "../../service/book-place.service";
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";
import {BookPlace} from "../../entity/BookPlace";

@Component({
  selector: 'app-broker-book-list',
  templateUrl: './broker-book-list.component.html',
  styleUrls: ['./broker-book-list.component.scss'],
  providers:[BookService,BookPlaceService]
})
export class BrokerBookListComponent implements OnInit {

  page = 0;
  books:Book[];
  user :User = this.cacheService.get('user');
  bookplaces:Array<BookPlace>;
  bookplace_id = -1;
  bookplaces_ngselect:Array<any>=[];

  constructor(private bookService:BookService,private bookPlaceService:BookPlaceService,private cacheService:CacheService) { }

  ngOnInit() {
    this.initBookplaces();
    // this.loadBooks(false);
  }

  initBookplaces()
  {
    this.bookPlaceService.getBookPlaceByUser(this.user).subscribe(bookplaces => {
      this.bookplaces = bookplaces;
      console.log(bookplaces);
      for(let bp of bookplaces)
      {
        let str = {label:bp.title,value:bp.id};
        this.bookplaces_ngselect.push(str);
      }
    });
  }

  public selectBookPlace(item:any):void {
    console.log('Selected value is: ', item.value);
    this.page = 0;
    this.bookplace_id = item.value;
    this.loadBooks(this.bookplace_id,false);
  }

  loadBooks(id:number,more:boolean)
  {
      this.bookService.getBooksByBookPlace(id,this.page).subscribe(books=>{
        if(!more)
        {
           this.books=books;
        }
        else
        {
            for(let book of books)
            {
              this.books.push(book);
            }
        }
      });
  }

}
