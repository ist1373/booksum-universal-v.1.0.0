import { Component, OnInit } from '@angular/core';
import {User} from "../../entity/User";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-broker-header',
  templateUrl: './broker-header.component.html',
  styleUrls: ['./broker-header.component.scss'],
  providers:[CacheService]
})
export class BrokerHeaderComponent implements OnInit {
  user:User;
  constructor(private cacheService:CacheService) { }

  ngOnInit() {
    this.user = this.cacheService.get('user');
  }
  checkUserAuthority(authority:string)
  {
    if(this.user != null)
    {
      for (let auth of this.user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }
}
