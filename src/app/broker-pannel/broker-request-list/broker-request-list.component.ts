import { Component, OnInit } from '@angular/core';
import {BookService} from "../../service/book.service";
import {BookPlaceService} from "../../service/book-place.service";
import {CacheService} from "ng2-cache-service";
import {BookPlace} from "../../entity/BookPlace";
import {UserService} from "../../service/user.service";
import {User} from "../../entity/User";
import {BookRequest, RequestState} from "../../entity/BookRequest";
import {BookRequestService} from "../../service/book-request.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-broker-request-list',
  templateUrl: './broker-request-list.component.html',
  styleUrls: ['./broker-request-list.component.scss'],
  providers:[BookService,BookPlaceService,BookRequestService]
})
export class BrokerRequestListComponent implements OnInit {
  bookplaces:Array<BookPlace>;
  bookplace_id = -1;
  bookplaces_ngselect:Array<any>=[];
  user : User = this.cacheService.get('user');
  bookRequests:Array<BookRequest>;
  page = 0;
  RequestState :typeof  RequestState = RequestState;
  requestStates_ngselect : Array<any>;
  test = '2';
  // requestStates :Array<any>
  constructor(private bookService:BookService,private bookPlaceService:BookPlaceService,
              private cacheService:CacheService,private bookRequestService:BookRequestService) { }

  // Accepted, Rejected, Pending, Canceled

  ngOnInit() {
    // this.requestStates = [
    //   {label:'آماده تحویل',value:0},
    //   {label:'لغو شده',value:1},
    //   {label:'در حال بررسی',value:2},
    //   {label:'کنسل شده',value:3},
    //   ];
    this.initBookplaces();
    this.requestStates_ngselect = [
        {label:'آماده تحویل',value:'Accepted'},
        {label:'لغو شده',value:'Rejected'},
        {label:'در حال بررسی',value:'Pending'},
        {label:'کنسل شده',value:'Canceled'},
    ];

  }


  changeRequestState(event,br:BookRequest,resText:string){
    br.requestState = event.value;

    console.log(br);
    this.bookRequestService.editBookRequestState(br.id,event.value,resText).subscribe(res=>{
      console.log(res);
    });
  }

  initBookplaces()
  {
    this.bookPlaceService.getBookPlaceByUser(this.user).subscribe(bookplaces => {
      this.bookplaces = bookplaces;
      console.log(bookplaces);
      for(let bp of bookplaces)
      {
        let str = {label:bp.title,value:bp.id};
        this.bookplaces_ngselect.push(str);
      }
    });
  }

  public selectBookPlace(item:any):void {
    console.log('Selected value is: ', item.value);
    this.page = 0;
    this.bookplace_id = item.value;
    this.loadRequests(this.bookplace_id,false);
  }

  loadRequests(id:number,more:boolean)
  {
    this.bookRequestService.getBookRequestsByBookPlace(id,this.page).subscribe(bookRequests=>{
      if(!more)
      {
        this.bookRequests = bookRequests;
        console.log(this.bookRequests);
      }
      else
      {
        for(let br of bookRequests)
        {
          this.bookRequests.push(br);
        }
      }
    });
  }

}
