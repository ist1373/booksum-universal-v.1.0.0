var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from "@angular/material";
import { CategoryService } from "./category-service";
import { Category } from "../entity/Category";
import { plainToClass } from "class-transformer";
import { Router } from "@angular/router";
import { MainBookletService } from "../main-booklet/main-booklet.service";
import { CacheService } from "ng2-cache-service";
var CategoryDialogComponent = CategoryDialogComponent_1 = (function () {
    function CategoryDialogComponent(router, dialogRef, categoryService, dialog, cacheService) {
        this.router = router;
        this.dialogRef = dialogRef;
        this.categoryService = categoryService;
        this.dialog = dialog;
        this.cacheService = cacheService;
        this.categories = [];
        this.categoryHeader = "";
        this.returnCatId = false;
        this.catId = 0;
    }
    CategoryDialogComponent.prototype.ngOnInit = function () {
        // this.categoryService.getParentsCategory().subscribe((data:Response)=> {
        //   this.categoryHeader = "دسته بندی";
        //   this.categories = plainToClass(Category, data.json());
        //   console.log(this.categories);
        // });
    };
    CategoryDialogComponent.prototype.openChildModal = function (id, title) {
        var _this = this;
        this.dialogRef.close();
        this.categoryService.getSubCategories(id).subscribe(function (data) {
            var cats = plainToClass(Category, data.json());
            if (cats.length != 0) {
                var newDialogRef = _this.dialog.open(CategoryDialogComponent_1, { height: '500px' });
                newDialogRef.componentInstance.categoryHeader = title;
                newDialogRef.componentInstance.categories = cats;
                newDialogRef.componentInstance.returnCatId = _this.returnCatId;
            }
            else {
                if (_this.returnCatId) {
                    var cat = new Category();
                    cat.id = id;
                    cat.title = title;
                    CategoryService.categoryObserver.next(cat);
                }
                else {
                    _this.cacheService.set("source", "fromCat");
                    _this.cacheService.set("catId", id);
                    _this.cacheService.set("title", title);
                    // if(this.router.url.substr(0,12)=="/booklet/cat")
                    // {
                    //   this.router.navigate(['/booklet/cat',title]);
                    //   window.location.reload();
                    // }
                    // else{
                    _this.router.navigate(['/booklet/cat', id, title]);
                }
            }
        });
    };
    return CategoryDialogComponent;
}());
CategoryDialogComponent = CategoryDialogComponent_1 = __decorate([
    Component({
        selector: 'app-category-dialog',
        templateUrl: './category-dialog.component.html',
        styleUrls: ['./category-dialog.component.scss'],
        providers: [CategoryService, MainBookletService, CacheService]
    }),
    __metadata("design:paramtypes", [Router, MdDialogRef, CategoryService,
        MdDialog, CacheService])
], CategoryDialogComponent);
export { CategoryDialogComponent };
var CategoryDialogComponent_1;
//# sourceMappingURL=category-dialog.component.js.map
