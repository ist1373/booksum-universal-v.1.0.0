import { Component, OnInit } from '@angular/core';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {HeaderComponent} from '../header/header.component';
import {CategoryService} from './category-service';
import {Category} from '../entity/Category';
import {plainToClass} from 'class-transformer';
import {Response} from '@angular/http';
import {Router} from '@angular/router';
import {BookletService} from '../service/booklet.service';
import {CacheService} from 'ng2-cache-service';
import {ExpireTime} from "../globals";

@Component({
  selector: 'app-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss'],
  providers: [CategoryService, BookletService, CacheService]
})
export class CategoryDialogComponent implements OnInit {

  constructor(private router: Router, public dialogRef: MdDialogRef<CategoryDialogComponent>, private categoryService: CategoryService,
              public dialog: MdDialog, private cacheService: CacheService) { }
  categories: Category[] = [];
  categoryHeader = '';
  returnCatId = false;
  catId= 0;

  ngOnInit() {
    // this.categoryService.getParentsCategory().subscribe((data:Response)=> {
    //   this.categoryHeader = "دسته بندی";
    //   this.categories = plainToClass(Category, data.json());
    //   console.log(this.categories);
    // });


  }

  openChildModal(id: number, title: string){
      this.dialogRef.close();
      if(!this.cacheService.exists('cat-'+id))
      {
        console.log("not cacheeeeeeed");
        this.categoryService.getSubCategories(id).subscribe((data: Response) => {
          const cats = plainToClass(Category, data.json());
          this.cacheService.set('cat-'+id,cats,{expires: Date.now() + ExpireTime});
          this.openCats(cats,id,title);
        });
      }
      else {
        const cats = this.cacheService.get('cat-'+id);
        this.openCats(cats,id,title);
      }

  }

  openCats(cats:Category[],id: number,title: string){
    if (cats.length != 0)
    {
      let config = new MdDialogConfig();
      config.height = '500px';
      const newDialogRef = this.dialog.open(CategoryDialogComponent, config);
      newDialogRef.componentInstance.categoryHeader = title;
      newDialogRef.componentInstance.categories = cats;
      newDialogRef.componentInstance.returnCatId = this.returnCatId;
    }
    else {
      if (this.returnCatId) //when we want to pick one category
      {
        const cat = new Category();
        cat.id = id;
        cat.title = title;
        CategoryService.categoryObserver.next(cat);
      }
      else
      {

        this.cacheService.set('source', 'fromCat');
        this.cacheService.set('catId', id) ;
        this.cacheService.set('title', title) ;
        this.router.navigate(['/book/cat', id, title.replace(/ /g,'-')]);

      }
    }
  }

}
