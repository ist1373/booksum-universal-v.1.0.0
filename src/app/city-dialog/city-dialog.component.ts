import {Component, Inject, OnInit} from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {LocationService} from "../service/location.service";
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";

@Component({
  selector: 'app-city-dialog',
  templateUrl: './city-dialog.component.html',
  styleUrls: ['./city-dialog.component.scss'],
  providers: [ CacheService,LocationService]
})
export class CityDialogComponent implements OnInit {

  provs = [];
  constructor(private cacheService:CacheService,private locationService:LocationService,private dialogRef: MdDialogRef<CityDialogComponent>) { }

  ngOnInit() {
    this.initActiveProvinces();
  }

  initActiveProvinces(){
    this.locationService.getActiveProvinces().subscribe(provs => {
      this.provs = provs;
    });
  }

  selectActiveProvince(id,title)
  {
    this.cacheService.set('activeProvince',id);
    this.cacheService.set('titleActiveProvince',title);

    this.dialogRef.close();
    window.location.reload();
  }
}
