import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ContactMessage} from "../entity/ContactMessage";
import {ContactMessageService} from "../service/contact-message.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {CustomValidators} from "ng2-validation";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  providers:[ContactMessageService]
})
export class ContactUsComponent implements OnInit {


  contactForm:FormGroup;
  email = new FormControl('',CustomValidators.email);
  content = new FormControl('');

  constructor(private contactService:ContactMessageService,public snackBar:MdSnackBar) {
    this.contactForm =new FormGroup({
      email:this.email,
      content:this.content
    });
  }

  saveContactMessage(){
    let contactMessage = new ContactMessage();
    contactMessage.content = this.content.value;
    contactMessage.email = this.email.value;
    this.contactService.saveContactMessage(contactMessage).subscribe((res)=>{
      if (res.json()['success'] == true)
      {
        this.openSnackBar();
        this.email.setValue('');
        this.content.setValue('');
      }
    });
  }


  ngOnInit() {
    window.scrollTo(0, 0);
  }

  openSnackBar() {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = ['success'];
    this.snackBar.open('پیام شما با موفقیت ارسال شد', '', conf);
  }
}
