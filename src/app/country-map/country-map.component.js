var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import * as $ from 'jquery';
import { Router } from "@angular/router";
import { CacheService } from "ng2-cache-service";
// import * as boot from 'mdbootstrap';
window["$"] = $;
window["jQuery"] = $;
var CountryMapComponent = (function () {
    function CountryMapComponent(cacheService, router) {
        this.cacheService = cacheService;
        this.router = router;
        this.city = "";
    }
    // @HostListener('document:keyup', ['$event'])
    // onKeyUp(ev:KeyboardEvent) {
    //   // do something meaningful with it
    //   console.log(`The user just pressed ${ev.key}!`);
    // }
    //
    CountryMapComponent.prototype.over = function (x) {
        this.city = x;
    };
    CountryMapComponent.prototype.ngOnInit = function () {
    };
    CountryMapComponent.prototype.selectCity = function () {
        var result = this.cacheService.set('city', [this.city]);
        this.router.navigate(['book/' + this.city]);
    };
    return CountryMapComponent;
}());
CountryMapComponent = __decorate([
    Component({
        selector: 'app-country-map',
        templateUrl: './country-map.component.html',
        styleUrls: ['./country-map.component.scss'],
        providers: [CacheService]
    }),
    __metadata("design:paramtypes", [CacheService, Router])
], CountryMapComponent);
export { CountryMapComponent };
//# sourceMappingURL=country-map.component.js.map
