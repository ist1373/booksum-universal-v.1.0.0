import {Component, OnInit, HostListener} from '@angular/core';


import {Router} from "@angular/router";
import {CacheService} from "ng2-cache-service";
// import * as boot from 'mdbootstrap';
// window["$"] = $;
// window["jQuery"] = $;


@Component({
  selector: 'app-country-map',
  templateUrl: './country-map.component.html',
  styleUrls: ['./country-map.component.scss'],
  providers: [CacheService]

})
export class CountryMapComponent implements OnInit {

  constructor(private cacheService: CacheService,private router:Router) {}

  city = "";
  // @HostListener('document:keyup', ['$event'])
  // onKeyUp(ev:KeyboardEvent) {
  //   // do something meaningful with it
  //   console.log(`The user just pressed ${ev.key}!`);
  // }
  //
  over(x)
  {
    this.city = x;
  }

  ngOnInit() {

  }
  selectCity(){
    let result: boolean = this.cacheService.set('city', [this.city]);
    this.router.navigate(['book/'+ this.city])
  }



}
