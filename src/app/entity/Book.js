var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import {Product} from "./Product";
import { Category } from "./Category";
import { UploadedFile } from "./UploadedFile";
import { City } from "./City";
import { Type } from "class-transformer/index";
import { User } from "./User";
import { CallInfo } from "./CallInfo";
/**
 * Created by iman on 10/02/2017.
 */
var Book = (function () {
    function Book() {
    }
    return Book;
}());
export { Book };
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], Book.prototype, "coverImage", void 0);
__decorate([
    Type(function () { return User; }),
    __metadata("design:type", User)
], Book.prototype, "user", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Book.prototype, "creationDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Book.prototype, "publishedDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Book.prototype, "archivedDate", void 0);
__decorate([
    Type(function () { return Category; }),
    __metadata("design:type", Category)
], Book.prototype, "category", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", Array)
], Book.prototype, "images", void 0);
__decorate([
    Type(function () { return City; }),
    __metadata("design:type", City)
], Book.prototype, "city", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Book.prototype, "publishYear", void 0);
__decorate([
    Type(function () { return CallInfo; }),
    __metadata("design:type", CallInfo)
], Book.prototype, "callInfo", void 0);
var BookState;
(function (BookState) {
    BookState[BookState["GoodUsed"] = 0] = "GoodUsed";
    BookState[BookState["BadUsed"] = 1] = "BadUsed";
    BookState[BookState["AlmostNew"] = 2] = "AlmostNew";
    BookState[BookState["New"] = 3] = "New";
})(BookState || (BookState = {}));
;
//# sourceMappingURL=Book.js.map