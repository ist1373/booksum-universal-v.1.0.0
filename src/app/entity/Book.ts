// import {Product} from "./Product";
import {Category} from './Category';
import {UploadedFile} from './UploadedFile';
import {City} from './City';
import {Tag} from './Tag';
import {Injectable} from '@angular/core';
import {Type} from 'class-transformer/index';
import {User} from './User';
import {University} from './University';
import {ProductState} from "./Content";
import {Store} from "./Store";
import {BookPlace} from "./BookPlace";


/**
 * Created by iman on 10/02/2017.
 */



export class Book  {

  id: number;
  uid:string;

  title: string;

  @Type(() => UploadedFile)
  coverImage: UploadedFile;


  @Type(() => Date)
  creationDate: Date;

  @Type(() => User)
  createdBy: User;

  @Type(() => Date)
  publishedDate: Date;

  @Type(() => Date)
  archivedDate: Date;


  @Type(() => Category)
  category: Category;

  @Type(() => Category)
  optionalCategories: Category[];

  @Type(() => UploadedFile)
  images: UploadedFile[];

  @Type(() => University)
  university: University;

  productState:ProductState;

  @Type(() => City)
  city: City;

  @Type(() => Comment)
  comments: Comment[];

  @Type(() => Tag)
  tags: Tag[];

  author: String ;

  translator: String ;

  publisher: string ;


  publishYear: number ;

  bookPrice: number;

  sellPrice: number;

  offPercentage: number;

  avgRate: number;

  description: String;

  bookState: BookState;

  tagState: TagState;

  weight: number;

  edition:string;

// call info
  email:string;

  phone:string;

  messengerId:string;

  @Type(() => Store)
  stores:Store[];

  @Type(() => BookPlace)
  bookPlaces:BookPlace[];

   infoType:InfoType;


    constructor() {
      this.images = [];
      this.tags = [];
      this.stores = [];
      this.bookPlaces =[];
    }
}


export enum  BookState{GoodUsed, BadUsed, NewUsed, New};


export enum TagState {
  Immediate, Rare, UnderPrice, Normal
}

export enum InfoType {
  Personal, Shop
}
