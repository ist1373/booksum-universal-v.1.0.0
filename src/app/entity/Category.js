var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { UploadedFile } from "./UploadedFile";
import { Type } from "class-transformer/index";
/**
 * Created by iman on 10/02/2017.
 */
var Category = (function () {
    function Category() {
    }
    return Category;
}());
export { Category };
__decorate([
    Type(function () { return Category; }),
    __metadata("design:type", Category)
], Category.prototype, "parent", void 0);
__decorate([
    Type(function () { return Category; }),
    __metadata("design:type", Array)
], Category.prototype, "subCategories", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], Category.prototype, "coverImage", void 0);
//# sourceMappingURL=Category.js.map