import {UploadedFile} from "./UploadedFile";
import {Type} from "class-transformer/index";

/**
 * Created by iman on 10/02/2017.
 */

export class Category {

  id:number;

  title:string;
  isSpecial:Boolean;

  @Type(() => Category)
  parent:Category;


  @Type(() => Category)
  subCategories:Category[];

  @Type(() => UploadedFile)
  coverImage:UploadedFile;

  childCount:number;

  constructor(){
    this.title = "";
    this.id = 0;
  }

}
