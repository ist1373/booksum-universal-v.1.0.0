/**
 * Created by iman on 10/02/2017.
 */
import {Injectable} from "@angular/core";
import {Province} from "./Province";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 10/02/2017.
 */

export class City {


    id:number;

    title:String;

    @Type(() => Province)
    province:Province;

}
