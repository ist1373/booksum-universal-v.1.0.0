
import {User} from "./User";
import {Product} from "./Product";
import {Type} from "class-transformer/index";
export class Comment {

  id:Number;

  rate:number;

  description:String;

  @Type(() => User)
  createdBy:User;

  @Type(() => Product)
  product:Product;

  @Type(() => Date)
  creationDate:Date;

  // commentState:CommentState;
  commentState:CommentState;
}

 export enum CommentState {Pending, Published, Canceled};
