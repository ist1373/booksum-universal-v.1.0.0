

import {User} from "./User";
import {UploadedFile} from "./UploadedFile";
import {Type} from "class-transformer";
import {University} from "./University";
import {City} from "./City";

export class Student extends User {


  deviceId:string;

  systemOS:string;

  systemVersion:string;

  systemDevice:string;

  shabaCode:string;

  nationalCode:string;

  postalCode:string;

  address:string;

  firstName:string;

  lastName:string;

  biography:string;



  @Type(() => UploadedFile)
  profileImage:UploadedFile;

  @Type(() => UploadedFile)
  nationalCardImage:UploadedFile;



  @Type(() => City)
  city:City;

  customerType:CustomerType;
}

export enum CustomerType {
  Seller, Broker
}
