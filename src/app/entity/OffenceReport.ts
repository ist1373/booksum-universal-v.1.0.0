
import {Product} from "./Product";
import {User} from "./User";
import {Type} from "class-transformer/index";
export class OffenceReport {

  id:number;

  @Type(() => Date)
  creationDate:Date;

  description:String;

  @Type(() => Product)
  product:Product;

  @Type(() => User)
  user:User;

  reportState:ReportState;

}
 enum ReportState {Seen, notSeen}
