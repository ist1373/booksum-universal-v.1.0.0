/**
 * Created by iman on 5/22/2017.
 */

export class ZarinpalPayment {

  MerchantID:string;
  Amount: number;
  CallbackURL:string;
  Description:string;

}
