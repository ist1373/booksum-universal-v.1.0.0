var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { User } from "./User";
import { UploadedFile } from "./UploadedFile";
import { Type } from "class-transformer/index";
/**
 * Created by iman on 20/02/2017.
 */
var Product = (function () {
    function Product() {
    }
    return Product;
}());
export { Product };
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], Product.prototype, "coverImage", void 0);
__decorate([
    Type(function () { return User; }),
    __metadata("design:type", User)
], Product.prototype, "user", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Product.prototype, "creationDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Product.prototype, "publishedDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Product.prototype, "archivedDate", void 0);
var ProductState;
(function (ProductState) {
    ProductState[ProductState["Evaluating"] = 0] = "Evaluating";
    ProductState[ProductState["Published"] = 1] = "Published";
    ProductState[ProductState["Canceled"] = 2] = "Canceled";
    ProductState[ProductState["NeedChange"] = 3] = "NeedChange";
    ProductState[ProductState["Archived"] = 4] = "Archived";
})(ProductState || (ProductState = {}));
//# sourceMappingURL=Product.js.map