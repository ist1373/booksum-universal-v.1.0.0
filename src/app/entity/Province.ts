import {City} from "./City";
/**
 * Created by iman on 10/02/2017.
 */
import {Injectable} from "@angular/core";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 10/02/2017.
 */

export class Province {

    id:number;

    title:String;

    @Type(() => City)
    cityList:City[];

}
