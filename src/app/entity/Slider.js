var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { User } from "./User";
import { UploadedFile } from "./UploadedFile";
import { Category } from "./Category";
import { Product } from "./Product";
import { Type } from "class-transformer/index";
/**
 * Created by iman on 16/02/2017.
 */
var Slider = (function () {
    function Slider() {
    }
    return Slider;
}());
export { Slider };
__decorate([
    Type(function () { return User; }),
    __metadata("design:type", User)
], Slider.prototype, "user", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Slider.prototype, "startDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Slider.prototype, "endDate", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], Slider.prototype, "image", void 0);
__decorate([
    Type(function () { return Category; }),
    __metadata("design:type", Category)
], Slider.prototype, "category", void 0);
__decorate([
    Type(function () { return Product; }),
    __metadata("design:type", Product)
], Slider.prototype, "product", void 0);
var SliderType;
(function (SliderType) {
    SliderType[SliderType["Book"] = 0] = "Book";
    SliderType[SliderType["Jozve"] = 1] = "Jozve";
    SliderType[SliderType["Category"] = 2] = "Category";
    SliderType[SliderType["Poster"] = 3] = "Poster";
})(SliderType || (SliderType = {}));
//# sourceMappingURL=Slider.js.map