import {User} from "./User";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 16/02/2017.
 */
export class TermAndCondition {


    id:number;

    @Type(() => Date)
    creationDate:Date;

    @Type(() => User)
    user:User;

    mainContent:String;

    keyContent:String;

}
