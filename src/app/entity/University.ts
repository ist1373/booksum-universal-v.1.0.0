import {City} from "./City";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 16/02/2017.
 */
export class University {


  id:number;


  title:string;

  @Type(() => City)
  city:City;

  constructor()
  {
    this.id = 0;
    this.title = "";
  }
}
