var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { UploadedFile } from "./UploadedFile";
import { Type } from "class-transformer/index";
import { Authority } from "./security/Authority";
var User = (function () {
    function User() {
    }
    return User;
}()); /**
 * Created by iman on 10/02/2017.
 */
export { User };
__decorate([
    Type(function () { return Authority; }),
    __metadata("design:type", Array)
], User.prototype, "authorities", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], User.prototype, "lastPasswordResetDate", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], User.prototype, "profileImage", void 0);
//# sourceMappingURL=User.js.map