
import {Category} from "../Category";
import {Transform, Type} from "class-transformer/index";


/**
 * Created by iman on 16/02/2017.
 */
export class CategorySection {
  title:String;
  @Type(() => Category)
  categoryList :Category[]; //Category[];
  position:number;
}
