import {Content} from "../Content";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 16/02/2017.
 */
export class ContentSection {

   title:String;

   loadMoreLink:String;

   @Type(() => Content)
   contentList:Content[];

   position:number;

}
