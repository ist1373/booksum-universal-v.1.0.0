import {UploadedFile} from "../UploadedFile";
import {Type, Exclude} from "class-transformer/index";
import {Product} from "../Product";
/**
 * Created by iman on 20/02/2017.
 */
export class PosterSection {
  @Type(() => UploadedFile)
  posterImage:UploadedFile;

  @Type(() => Product)
  product:Product;

  position:number;

  posterType:PosterType;

  viewAction:String;

  viewUrl:String;

}

enum PosterType {
  Book
    ,
  Content
    ,
  Category
    ,
  Poster
}
