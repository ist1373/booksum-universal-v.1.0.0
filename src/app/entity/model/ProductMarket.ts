import {CategorySection} from "./CategorySection";
import {ContentSection} from "./ContentSection";
import {BookSection} from "./BookSection";
import {PosterSection} from "./PosterSection";
import {Type} from "class-transformer/index";
import {SliderSection} from "./SliderSection";
import {SpecialContent} from "./SpecialContent";
import {SpecialBook} from "./SpecialBook";

export class ProductMarket {

  @Type(() => SliderSection)
  sliderSection:SliderSection;
  @Type(() => CategorySection)
  categorySections :CategorySection[];
  @Type(() => SpecialContent)
  specialContents:SpecialContent[];
  @Type(() => SpecialBook)
  specialBooks:SpecialBook[];
  @Type(() => ContentSection)
  contentSections:ContentSection[];
  @Type(() => BookSection)
  bookSections :BookSection[];
  @Type(() => PosterSection)
  posterSections:PosterSection[];
  sectionSize:number;

}
