import {Book} from "../Book";
import {UploadedFile} from "../UploadedFile";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 20/02/2017.
 */
export class SpecialBook {
   @Type(() => UploadedFile)
   posterImage:UploadedFile;

   @Type(() => UploadedFile)
   posterImageWeb:UploadedFile;

   @Type(() => Book)
   book:Book;

   position:number;
}
