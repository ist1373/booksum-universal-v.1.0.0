/**
 * Created by iman on 5/22/2017.
 */

export class Authority {

  id:number;
  name: string;
  // email:string;
  // authority:string;

}

export enum AuthorityName {
  ROLE_USER, ROLE_ADMIN, ROLE_CONTENT_ADDER, ROLE_SELLER, ROLE_BROKER
}
