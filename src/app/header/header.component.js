var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { CategoryService } from "../category-dialog/category-service";
import { Category } from "../entity/Category";
import { plainToClass } from "class-transformer/index";
import { MdDialog } from "@angular/material";
import { CategoryDialogComponent } from "../category-dialog/category-dialog.component";
import { LoginRegisterComponent } from "../login-register/login-register.component";
import { UserService } from "../user.service";
import { AppContextUrl } from "../globals";
import { BooksumCacheService } from "../booksum-cache.service";
import { CacheService } from "ng2-cache-service";
var HeaderComponent = (function () {
    function HeaderComponent(booksumCacheService, userService, cacheService, router, categoryService, dialog) {
        this.booksumCacheService = booksumCacheService;
        this.userService = userService;
        this.cacheService = cacheService;
        this.router = router;
        this.categoryService = categoryService;
        this.dialog = dialog;
        this.categories = [];
        this.categoryHeader = "";
        this.base = AppContextUrl;
        this.city = this.cacheService.get('city');
    }
    HeaderComponent.prototype.ngOnDestroy = function () {
        this._subscription.unsubscribe();
    };
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._subscription = this.userService.userChange.subscribe(function (user) {
            _this.user = user;
        });
        if (this.cacheService.exists("user")) {
            this.user = this.cacheService.get("user");
            console.log(this.user);
        }
    };
    HeaderComponent.prototype.getCity = function () {
        if (this.city == null) {
            this.router.navigate(['selectcity']);
        }
        else {
            this.router.navigate(['book/' + this.city]);
        }
    };
    HeaderComponent.prototype.isActive = function () {
        return this.router.isActive("book/" + this.city, true);
    };
    HeaderComponent.prototype.openModal = function () {
        var _this = this;
        this.categoryService.getParentsCategory().subscribe(function (data) {
            var dialogRef = _this.dialog.open(CategoryDialogComponent);
            dialogRef.componentInstance.categories = plainToClass(Category, data.json());
            dialogRef.componentInstance.categoryHeader = "دسته بندی";
        });
    };
    HeaderComponent.prototype.logOut = function () {
        this.cacheService.remove("user");
        this.cacheService.remove("token");
        this.userService.userChange.next(null);
        window.location.reload();
    };
    HeaderComponent.prototype.openLoginRegisterModal = function () {
        var dialogRef = this.dialog.open(LoginRegisterComponent, { width: '400px' });
        // dialogRef.afterClosed().subscribe(()=>{
        //     if(dialogRef.componentInstance.status==200)
        //     {
        //       this.userService.getFinalUser().subscribe((res)=>{
        //         this.user = plainToClass<User,Object>(User,res.json());
        //
        //         this.cacheService.set("user",this.user,{expires: Date.now() + ExpireTime});
        //       });
        //     }
        //
        // });
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.scss'],
        providers: [CacheService, CategoryService, UserService, BooksumCacheService]
    }),
    __metadata("design:paramtypes", [BooksumCacheService, UserService,
        CacheService, Router, CategoryService,
        MdDialog])
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map
