import {Component, OnInit, ViewChild, ElementRef, Output, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
// import { Overlay } from 'angular2-modal';
// import { Modal } from 'angular2-modal/plugins/bootstrap';
import {CategoryService} from '../category-dialog/category-service';
import {Category} from '../entity/Category';
import {plainToClass} from 'class-transformer/index';
import {Response} from '@angular/http';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {CategoryDialogComponent} from '../category-dialog/category-dialog.component';
import {LoginRegisterComponent} from '../login-register/login-register.component';
import {UserService} from '../service/user.service';
import {Content} from '../entity/Content';
import {User} from '../entity/User';
import {AppContextUrl, ExpireTime, FileContextUrl} from '../globals';
import {BooksumCacheService} from '../service/booksum-cache.service';
import {CacheService} from 'ng2-cache-service';
import {Province} from "../entity/Province";
import {LocationService} from "../service/location.service";
import {CityDialogComponent} from "../city-dialog/city-dialog.component";
// import {TourService} from 'ngx-tour-core';



declare var jQuery: any;



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [CacheService, CategoryService, UserService, BooksumCacheService,LocationService]
  }
)
export class HeaderComponent implements OnInit {
  private city: any|null;
  categories: Category[] = [];
  titleActiveProvince = '';
  categoryHeader= '';
  user: User;
  token = this.cacheService.get('token');
  base= AppContextUrl;
  baseFile = FileContextUrl;
  _subscription: any;


  constructor(private booksumCacheService: BooksumCacheService, private userService: UserService,
              private cacheService: CacheService, private router: Router, private categoryService: CategoryService,
              public dialog: MdDialog,private locationService:LocationService ) {
    this.city  = this.cacheService.get('city');

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  ngOnInit() {
    this._subscription = this.userService.userChange.subscribe((user) => {

      this.user = user;
    });
    if (this.cacheService.exists('user'))
    {
      this.user = this.cacheService.get('user');
      console.log(this.user);
    }
    // this.cacheService.remove('titleActiveCity');
    if (this.cacheService.exists('titleActiveProvince'))
    {
      this.titleActiveProvince = this.cacheService.get('titleActiveProvince');
    }
    else
    {
      this.openCityModal();
    }




  }


  openCatModal(){
    let config = new MdDialogConfig();
    if(!this.cacheService.exists('parent-cat'))
    {
      this.categoryService.getParentsCategory().subscribe((data: Response) => {
        const dialogRef = this.dialog.open(CategoryDialogComponent);
        dialogRef.componentInstance.categories = plainToClass(Category, data.json());
        this.cacheService.set('parent-cat',dialogRef.componentInstance.categories,{expires: Date.now() + ExpireTime});
        dialogRef.componentInstance.categoryHeader = 'دسته بندی';
      });
    }
    else
    {
      const dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.categories = this.cacheService.get('parent-cat');
      dialogRef.componentInstance.categoryHeader = 'دسته بندی';
    }
  }
  openCityModal(){
    const dialogRef = this.dialog.open(CityDialogComponent);
    dialogRef.afterClosed().subscribe(res=>{
      this.titleActiveProvince = this.cacheService.get('titleActiveProvince');

    });
  }

  logOut(){
    this.cacheService.remove('user');
    this.cacheService.remove('token');
    this.userService.userChange.next(null);
    window.location.reload();
  }


  openLoginRegisterModal(isSignIn: boolean)
  {

    let config = new MdDialogConfig();
    config.width = '400px';

    const dialogRef = this.dialog.open(LoginRegisterComponent);
    dialogRef.componentInstance.signInTap = isSignIn;

  }




  checkUserAuthority(authority:string):boolean
  {
    if(this.user != null)
    {
      for (let auth of this.user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }




}



