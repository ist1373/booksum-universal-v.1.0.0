import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {UserService} from '../service/user.service';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {CacheService} from 'ng2-cache-service';
import {ExpireTime} from '../globals';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {plainToClass} from 'class-transformer';
import {User} from '../entity/User';
import {Student} from "../entity/Student";
import {Ng2DeviceService} from "ng2-device-detector";
import {AppComponent} from "../app.component";


@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.scss'],
  providers: [UserService, CacheService]
})
export class LoginRegisterComponent implements OnInit {
   logmessage = '';
   regmessage = '';
   status = 401;
   complexForm : FormGroup;

   loginForm : FormGroup;
   logemail = new FormControl('' );
   logpass = new FormControl('');

   signInTap = true;

   pass = new FormControl('', Validators.required);
   passrep = new FormControl('', CustomValidators.equalTo(this.pass));
   email = new FormControl('', CustomValidators.email);
   rememberCheckBox = false;
  // uname = new FormControl('', Validators.minLength(6));

  constructor(private deviceService: Ng2DeviceService,public snackBar: MdSnackBar, private cacheService: CacheService, private userService: UserService, public dialogRef: MdDialogRef<LoginRegisterComponent>) {

    this.complexForm = new FormGroup({
      pass: this.pass,
      passrep: this.passrep,
      email: this.email
    });

    this.loginForm = new FormGroup({
      logemail : this.logemail,
      logpass : this.logpass
    });
  }


  ngOnInit() {
     this.rememberCheckBox = this.cacheService.get('remember');
     if(this.rememberCheckBox)
     {
       this.logemail.setValue(this.cacheService.get('rememberEmail'));
       this.logpass.setValue(this.cacheService.get('rememberPassword'));
     }
  }

  onChangeRemember(){
    this.rememberCheckBox = !this.rememberCheckBox;
    this.cacheService.set('remember',this.rememberCheckBox);

  }



  login()
  {
    if(this.rememberCheckBox)
    {
      this.cacheService.set('rememberEmail',this.logemail.value);
      this.cacheService.set('rememberPassword',this.logpass.value);
    }
    this.userService.login(this.logemail.value, this.logpass.value).subscribe((res) => {
      this.cacheService.set('token', res.json()['token'], {expires: Date.now() + ExpireTime});
      this.status = 200;
      this.getUser();
    }, (error) => {
      this.logmessage = 'پست الکترونیک یا رمز عبور اشتباه است.';
    });
  }

  getUser()
  {
    this.userService.getUser().subscribe((res) => {
      let user = plainToClass<Student, Object>(Student, res.json());
      this.cacheService.remove('user');
      this.cacheService.set('user', user, {expires: Date.now() + ExpireTime});
      console.log(user);
      this.dialogRef.close();
      window.location.reload();
    });
  }




  register(email: string, pass: string, value: any)
  {
    let user  = new Student();
    user.password = pass;
    user.email = email;
    user.username = email;
    user.systemOS = this.deviceService.os_version;
    user.systemVersion = this.deviceService.browser + "-" + this.deviceService.browser_version
    user.systemDevice = this.deviceService.device;


    console.log(value);

    this.userService.registerFinalUser(user).subscribe((res) => {
      console.log(res);
      if (res.json()['success'] != true)
      {

        this.regmessage = res.json()['message'];
      }
      else
      {
        this.dialogRef.close();
        this.openSnackBar();
      }

    }, (error) => {
       this.regmessage = error.toString();
    });
  }


  openSnackBar() {
    let conf = new MdSnackBarConfig();


    conf.duration = 5000;
    conf.extraClasses = ['success'];
    this.snackBar.open('ثبت نام شما با موفقیت انجام شد.برای فعال سازی حساب به پست الکترونیک خود مراجعه کنید', '', conf);
  }
}
