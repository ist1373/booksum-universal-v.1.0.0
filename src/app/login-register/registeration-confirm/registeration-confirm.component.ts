import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../service/user.service";
import {CacheService} from "ng2-cache-service";
import {ExpireTime} from "../../globals";
import {plainToClass} from "class-transformer";
import {Student} from "../../entity/Student";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-registeration-confirm',
  templateUrl: './registeration-confirm.component.html',
  styleUrls: ['./registeration-confirm.component.scss'],
  providers:[UserService,CacheService,UserService]
})
export class RegisterationConfirmComponent implements OnInit {

  message = "ثبت نام شما ناموفق بود";
  success = false;
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private router:Router,private activatedRoute: ActivatedRoute,private userService:UserService,private cacheService:CacheService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.userService.registerationConfirm(params['token']).subscribe(res=>{
        if(res.json()['success'] == false)
        {
          this.success = false;
        }
        else
        {
          this.success = true;
          this.message = 'حساب شما با موفقیت فعال شد.';
          if (isPlatformBrowser(this.platformId)) {
            var i =5;
            setInterval(()=>{
              i--
              this.message = 'حساب شما با موفقیت فعال شد.شما '+ i + ' ثانیه دیگر به صفحه اصلی منتقل می شوید';
              if(i<=0)
              {
                let token = res.json()['message'];
                this.cacheService.set('token', token, {expires: Date.now() + ExpireTime});
                this.getUser();
              }
            },1000)
          }

        }
      });
    });
  }

  getUser()
  {
    this.userService.getUser().subscribe((res) => {
      let user = plainToClass<Student, Object>(Student, res.json());
      this.cacheService.remove('user');
      this.cacheService.set('user', user, {expires: Date.now() + ExpireTime});
      this.router.navigate(['']);
      },
      (error)=>{
        this.router.navigate(['']);
      });
  }

}
