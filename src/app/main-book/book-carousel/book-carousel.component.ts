import {Component, OnInit, Input} from '@angular/core';
import {BookSection} from "../../entity/model/BookSection";
import {CacheService} from "ng2-cache-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-book-carousel',
  templateUrl: './book-carousel.component.html',
  styleUrls: ['./book-carousel.component.scss']
})
export class BookCarouselComponent implements OnInit {
  @Input() car_id:string ;
  @Input() bookSection:BookSection ;


  constructor(private cacheService :CacheService,private router:Router) {

  }
  ngOnInit() {

  }

  loadMore()
  {
    this.cacheService.set("source", "fromUrl");
    this.cacheService.set("listFromUrl", this.bookSection.loadMoreLink.toString());
    this.cacheService.set("title", this.bookSection.title);
    this.router.navigate(['/book/list',this.bookSection.title.replace(/ /g,'-')]);
  }

}
