import {Component, HostListener, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AppContextUrl, ExpireTime, FileContextUrl} from "../../globals";
import {ProgressHttp} from "angular-progress-http";
import {MdDialog, MdDialogConfig, MdSnackBar, MdSnackBarConfig} from "@angular/material";

import {UserService} from "../../service/user.service";
import {User} from "../../entity/User";
import {CacheService} from "ng2-cache-service";
import {CookieService} from "ng2-cookies";
import {GalleryService} from "ng-gallery";
import {Tag} from "../../entity/Tag";
import {Comment} from "../../entity/Comment";
import {CommentsService} from "../../service/comments.service";
import {BookService} from "../../service/book.service";
import {Book, BookState, InfoType} from "../../entity/Book";
import {IStarRatingOnRatingChangeEven} from "angular-star-rating";
import {BookPlaceDialogComponent} from "./book-place-dialog/book-place-dialog.component";
import {StoreDialogComponent} from "./store-dialog/store-dialog.component";
import {Meta} from "@angular/platform-browser";
import {MetaService} from "@ngx-meta/core";
import {isPlatformBrowser} from "@angular/common";

// import {MetaService} from "@ngx-meta/core";






@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss'],
  providers:[BookService,UserService,CacheService,CookieService,CommentsService]
})
export class BookDetailsComponent implements OnInit {

  BookState:typeof BookState = BookState;
  InfoType:typeof InfoType = InfoType;
  belongToShop = false;
  book = new Book();
  showPhone = false;
  showEmail = false;
  showMessenger = false;
  comments: Comment[] = [];
  otherUserBooks:Book[] = [];
  base = AppContextUrl;
  baseFile = FileContextUrl;
  token = this.cacheService.get("token");
  user:User = this.cacheService.get("user");
  userComment = new Comment();
  images = [
  ];


  bookStates:Array<any>;

  page = 0;
  moreElement;
  noLoadMore=false;

  constructor(private userService:UserService,private cacheService: CacheService,private bookService: BookService,
              private router: Router, private activatedRoute: ActivatedRoute,public dialog: MdDialog,private gallery: GalleryService,
              private http: ProgressHttp,private commentService:CommentsService,public snackBar: MdSnackBar,
              private readonly metaService: MetaService,@Inject(PLATFORM_ID) private platformId: Object) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.loadBook(params['uid']);
    });
    //

    // this.gallery.load(this.images);
    // this.gallery.set(0);
    this.bookStates = [
      'دست دوم تمیز',
      'دست دوم مستهلک',
      'تقریبا نو',
      'نو'
    ];
    if(isPlatformBrowser(this.platformId))
    {
      this.moreElement = <HTMLElement>document.getElementById('more');
    }
  }

  loadBook(id: string) {
    this.otherUserBooks = [];
    this.images = [];
    if(isPlatformBrowser(this.platformId))
    {
      window.scrollTo(0, 0);
    }

    this.bookService.getBookByUID(id).subscribe((book) => {
      this.book = book;
      if(this.book.stores.length>0)
      {
        this.belongToShop = true;
      }

      this.metaService.setTitle(' کتاب '+this.book.title.toString() + ' ' + this.book.author.toString() + ' ' + (this.book.translator!=null?this.book.translator.toString():'') ,true);
      this.metaService.setTag( 'og:image',this.baseFile + 'fid=' + this.book.coverImage.id);
      this.metaService.setTag('og:description',this.book.category.title + '\n' + this.book.author.toString() + '\n' + this.book.city.title);
      this.metaService.setTag('og:locale','fa_IR');
      this.metaService.setTag('og:image:width','200px');
      this.metaService.setTag('og:image:height','300px');


      console.log(book);
      this.initSampleImages();
      this.loadUserBooks(book)
    });
  }


  // loadCallInfo(id:number)
  // {
  //   this.bookService.getCallInfoByBook(id).subscribe(callinfo => {
  //     this.callInfo = callinfo;
  //   })
  // }
  getBookState(bookstate:BookState){
    console.log(bookstate);
    if(bookstate == BookState.GoodUsed){
      return 'دست دوم تمیز';
    }
  }

  loadUserBooks(book:Book)
  {
    this.bookService.getBooksByUser(book.createdBy.uid,this.page).subscribe(books =>{
      for(let b of books)
      {
        this.otherUserBooks.push(b);
      }

    });
  }

  onShowPhone()
  {
    this.showPhone = true;
    var copyTextarea = <HTMLTextAreaElement>document.querySelector('#phoneText');
    copyTextarea.select();
    try {
      var successful = document.execCommand('copy');
      this.openSnackBar("شماره تلفن با موفقیت copy شد.می توانید آنرا در محل مورد نظر paste کنید." ,'success');
    }catch (err) {
    }
  }
  onShowEmail()
  {
    this.showEmail = true;
    var copyTextarea = <HTMLTextAreaElement>document.querySelector('#emailText');
    copyTextarea.select();
    try {
      var successful = document.execCommand('copy');
      this.openSnackBar("پست الکترونیکی با موفقیت copy شد.می توانید آنرا در محل مورد نظر paste کنید." ,'success');
    }catch (err) {
    }
  }
  onShowMessenger()
  {
    this.showMessenger = true;
    var copyTextarea = <HTMLTextAreaElement>document.querySelector('#messengerText');
    copyTextarea.select();
    try {
      var successful = document.execCommand('copy');
      this.openSnackBar("شناسه پیام رسان با موفقیت copy شد.می توانید آنرا در محل مورد نظر paste کنید." ,'success');
    }catch (err) {
    }
  }

  initSampleImages()
  {
    for(let image of this.book.images)
    {
      let i={
        src:FileContextUrl+'fid='+image.id,
        thumbnail: FileContextUrl+'fid='+image.id,
        text: image.description
      };
      this.images.push(i);
    }
    this.gallery.load(this.images);
  }

  openStores()
  {
    let conf = new MdDialogConfig();
    conf.height = '600px';
    let dilogRef = this.dialog.open(StoreDialogComponent,conf);
    dilogRef.componentInstance.stores = this.book.stores;
  }

  openBookPlaces(){
    let conf = new MdDialogConfig();
    conf.height = '600px';
    const dialogRef = this.dialog.open(BookPlaceDialogComponent,conf);
    dialogRef.componentInstance.bookPlaces = this.book.bookPlaces;
    dialogRef.componentInstance.book = this.book;
  }

  bookByTag(tag:Tag)
  {
    this.router.navigate(["book","tag",tag.id]);
  }


  openSnackBar(message:string,cssClass:string) {
    var conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,"",conf);
  }


  onRatingChange = ($event:IStarRatingOnRatingChangeEven) => {
    console.log('onRatingUpdated $event: ', $event);
    this.userComment.rate = $event.rating;
  };


  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    if(this.checkVisible(this.moreElement))
    {
      if(!this.noLoadMore)
      {
        this.page++;
        this.loadUserBooks(this.book);
        this.noLoadMore = true;
        console.log('load more!')
      }

    }
    else
    {
      this.noLoadMore = false;
    }
  }
  checkVisible(el) {
    if(isPlatformBrowser(this.platformId))
    {
      var elemTop = el.getBoundingClientRect().top;
      var elemBottom = el.getBoundingClientRect().bottom;
      var isVisible = (elemBottom <= window.innerHeight);
      return isVisible;
    }
  }





}
