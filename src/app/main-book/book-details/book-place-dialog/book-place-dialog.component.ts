import { Component, OnInit } from '@angular/core';
import {MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {CategoryDialogComponent} from "../../../category-dialog/category-dialog.component";
import {BookPlace} from "../../../entity/BookPlace";
import {AppContextUrl, FileContextUrl} from "../../../globals";
import {Book} from "../../../entity/Book";
import {BookRequest} from "../../../entity/BookRequest";
import {BookRequestService} from "../../../service/book-request.service";

@Component({
  selector: 'app-book-place-dialog',
  templateUrl: './book-place-dialog.component.html',
  styleUrls: ['./book-place-dialog.component.scss'],
  providers:[BookRequestService]
})
export class BookPlaceDialogComponent implements OnInit {

  showRequest = -1;
  bookPlaces:BookPlace[];
  book:Book;
  base = AppContextUrl;
  baseFile = FileContextUrl;
  zoom: number = 12;
  constructor( public dialogRef: MdDialogRef<CategoryDialogComponent>,public snackBar:MdSnackBar,private bookRequestService:BookRequestService) { }

  ngOnInit() {
    console.log(this.bookPlaces);
  }

  sendRequest(desc:string,bp:BookPlace)
  {
    let request = new BookRequest();
    request.book = this.book;
    request.bookPlace = bp;
    console.log(desc);

    this.bookRequestService.addBookRequest(request).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
        this.openSnackBar('رزرو کتاب با موفقیت انجام شد','success');
      else
        this.openSnackBar(res.json()['message'],'error');
    });

  }


  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }

}
