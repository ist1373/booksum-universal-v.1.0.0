import { Component, OnInit } from '@angular/core';
import {Store} from "../../../entity/Store";
import {AppContextUrl, FileContextUrl} from "../../../globals";

@Component({
  selector: 'app-store-dialog',
  templateUrl: './store-dialog.component.html',
  styleUrls: ['./store-dialog.component.scss']
})
export class StoreDialogComponent implements OnInit {

  stores:Array<Store>;
  base = AppContextUrl;
  baseFile = FileContextUrl;
  zoom: number = 12;
  // lat: number = 32.653000;
  // lng: number = 51.668237;


  constructor() { }

  ngOnInit() {
    console.log(this.stores)
  }

}
