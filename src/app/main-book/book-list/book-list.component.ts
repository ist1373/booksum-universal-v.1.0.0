import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Content} from "../../entity/Content";
import {CacheService} from "ng2-cache-service";
import {ItemSizePerPage} from "../../globals";
import {DOCUMENT} from "@angular/platform-browser";
import {FormControl, FormGroup} from "@angular/forms";
import {CategoryService} from "../../category-dialog/category-service";
import {MdDialog} from "@angular/material";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {plainToClass} from "class-transformer";
import {Category} from "../../entity/Category";
import {Http,Response} from "@angular/http";
import {University} from "../../entity/University";
import {UniversityService} from "../../service/university.service";
import {tick} from "@angular/core/testing";
import {Book, BookState} from "../../entity/Book";
import {BookService} from "../../service/book.service";
import {LocationService} from "../../service/location.service";
import {City} from "../../entity/City";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'],
  providers:[BookService,CacheService,CategoryService,UniversityService,LocationService]
})
export class BookListComponent implements OnInit {

  books:Book[];
  listFromUrl = "";
  catid=0;
  tagid = 0;
  title="";
  type= "";
  page = 0;

  universities : Array<any>=[];
  provinces : Array<any>=[];
  cities : Array<any>=[];
  bookStates:Array<any> = [];

  searchForm:FormGroup;
  titleSearch = new FormControl('');
  authorSearch = new FormControl('');
  translatorSearch = new FormControl('');
  publisherSearch = new FormControl('');
  publishYearSearch = new FormControl('');
  catSearch = new FormControl('');
  categorySearch:Category;
  universitySearch:University;
  bookStateSearch:BookState;
  citySearch:City;

  scrollOffsetToLoad ;

  constructor(private activatedRoute:ActivatedRoute,private bookService:BookService,private universityService:UniversityService,
              private cacheService:CacheService,private categoryService:CategoryService,private dialog:MdDialog,private locationService:LocationService) {
    this.searchForm = new FormGroup({
      title: this.titleSearch,
      author:this.authorSearch,
      translator:this.translatorSearch,
      publisher:this.publisherSearch,
      publishYear:this.publishYearSearch,
      catSearch:this.catSearch
    });

    this.categorySearch = new Category();
    this.universitySearch = new University();
    this.citySearch = new City();
  }

  ngOnInit() {
    this.books = [];
    this.bookStates = [
      {value: '0', label: 'دست دوم تمیز'},
      {value: '1', label: 'دست دوم مستهلک'},
      {value: '2', label: 'تقریبا نو'},
      {value: '3', label: 'نو'}
    ];
    console.log(this.cacheService.get("source"));
    this.activatedRoute.url.subscribe((url)=>{
      this.page = 0;
      this.scrollOffsetToLoad = 300;
      console.log("seeeeeeeen");

      if(url[1].toString()=="list")
      {
        this.type = "list";
        var temp = this.cacheService.get("listFromUrl").toString();
        this.listFromUrl = temp.substring(0,temp.length-2);
        this.bookService.getBooksByURL(this.listFromUrl+this.page+'&size='+ItemSizePerPage).subscribe((newbooks)=>{
          this.books = newbooks;
        });
        this.title = this.cacheService.get("title").toString();
      }
      else if(url[1].toString()=="cat") {
        this.type = "cat";
        this.activatedRoute.params.subscribe((params: Params) => {
          this.catid = params['id'];
          this.title = params['name'];

          this.loadByCat(this.catid,this.page,false);

        });
      }
      else if(url[1].toString()=="tag") {
        this.type = "tag";
        this.activatedRoute.params.subscribe((params: Params) => {
          this.tagid = params['id'];
          this.loadByTag(this.tagid,this.page,false);
        });
      }
      else if(url[1].toString()=="search") {
        this.type = "search";
        this.scrollOffsetToLoad = 600;
        this.initUniversirsities();
        this.initProvinces();
      }


    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.categorySearch = cat;
      console.log(cat);
    });
    this.catSearch.valueChanges.subscribe(val=>{
      if(val.length==0)
      {
        let cat = new Category();
        cat.id = 0;
        this.categorySearch = cat;
      }
    });
  }


  loadByCat(catId:number,page:number,more:boolean)
  {
    this.bookService.getBooksByCategory(catId,page).subscribe((contents)=>{
      if(!more)
      {
        this.books = contents;
      }
      else
      {
        for(let content of contents)
        {
          this.books.push(content);
        }
        // this.cacheService.set('cacheBooks',this.books,{expires: Date.now() + 60000});
        // this.cacheService.set('cacheYOffset',this.scrollOffsetToLoad,{expires: Date.now() + 60000});
        // this.cacheService.set('cachePage',this.page,{expires: Date.now() + 60000});
      }

    });
  }
  loadByTag(tagId:number,page:number,more:boolean)
  {
    this.bookService.getBooksByTag(tagId,this.page).subscribe((contents)=>{
      if(!more)
      {
        this.books = contents;
      }
      else
      {
        for(let content of contents)
        {
          this.books.push(content);
        }
      }
    });
  }


  @HostListener('window:scroll', ['$event'])
  doSomething(event) {
    // console.log(window.pageYOffset);
    if(this.scrollOffsetToLoad <= window.pageYOffset)
    {

      this.loadMore();
      this.scrollOffsetToLoad += 600;
    }
  }

  loadMore(){
    if(this.type == "list")
    {
      this.page++;
      this.bookService.getBooksByURL(this.listFromUrl+this.page+'&size='+ItemSizePerPage).subscribe((newbooks:Book[])=>{
        for(let book of newbooks)
        {
          this.books.push(book);
        }
      });
    }
    else if(this.type == "cat")
    {
      this.page++;
      this.loadByCat(this.catid,this.page,true);
    }
    else if(this.type == "tag")
    {
      this.page++;
      this.loadByTag(this.tagid,this.page,true);
    }
    else if(this.type == "search")
    {
      this.page++;
      this.findBooks(true);
    }
  }

  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }

  initUniversirsities()
  {
    this.universityService.getUniversities().subscribe((unis:University[])=>{
      for(let uni of unis)
      {
        this.universities.push({value:uni.id,label:uni.title.toString()});
      }
    });
    this.cacheService.set("unis",this.universities);
  }

  public selectUniversity(item:any):void {
    let uni = new University();
    this.universitySearch.id = item.value;
  }


  initProvinces(){
    this.locationService.getProvinces().subscribe(provs =>{
      for(let prov of provs)
      {
        this.provinces.push({value:prov.id,label:prov.title.toString()})
      }
    })
  }
  public selectProvince(item:any):void {
    console.log('Selected value is: ', item.value);
    this.cities = [];
    this.getCitis(item.value);
  }

  selectBookState(item:any)
  {
    this.bookStateSearch = item.value
  }



  getCitis(provinceId:number){

    this.locationService.getCities(provinceId).subscribe(cities =>{
      console.log(cities);
      for(let city of cities)
      {
        this.cities.push({value:city.id,label:city.title.toString()})
      }
    });
  }

  public selectCity(item:any):void {
    console.log('Selected value is: ', item.value);
    let city = new City();
    city.id = item.value;
    this.citySearch = city;
  }



  // findBook(title:string,author:string,translator:string,publisher:string,publishYear:string,bookState:BookState,universityId:number,categoryId:number,cityId:number, page:number)

  findBooks(more:boolean){
    console.log('page:' + this.page);
    if(!more) this.page = 0 ;
    this.bookService.findBook(this.titleSearch.value,this.authorSearch.value,this.translatorSearch.value,this.publisherSearch.value,this.publishYearSearch.value,this.bookStateSearch,this.universitySearch.id,this.categorySearch.id,this.citySearch.id,this.page)
      .subscribe((contents)=>{
        if(!more)
        {

          this.books = contents;
        }
        else {
          for(let b of contents)
          {
            this.books.push(b);
          }
        }
      });
  }
}
