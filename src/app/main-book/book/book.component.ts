import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from "../../entity/Book";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {MdDialog, MdDialogConfig} from "@angular/material";
import {BrokerSelectDialogComponent} from "../../broker-pannel/broker-add-book/broker-select-dialog/broker-select-dialog.component";
import {Subscription} from "rxjs/Subscription";
import {ProductState} from "../../entity/Content";
import {BookService} from "../../service/book.service";
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";




@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  providers:[BookService]
})
export class BookComponent implements OnInit {
  @Input() book:Book;
  baseUrl = AppContextUrl;
  baseFile = FileContextUrl;
  bookStates:Array<any>;
  ProductState: typeof ProductState = ProductState;
  @Input() forCarousel = false;
  @Output() onChangeState: EventEmitter<Book> = new EventEmitter();
  @Input() forBroker:boolean = false;
  @Input() forAdmin:boolean = false;
  @Input() forSeller:boolean = false;

  @Input() editable:boolean = false;

  constructor(private dialog:MdDialog,private bookService:BookService,private cacheService:CacheService) {
    this.bookStates = [
      {value: '0', label: 'درحال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'لغو شده'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '4', label: 'آرشیو شده'}
    ];
  }

  ngOnInit() {

  }
  selectBookplaceDialog(){
    let dialogConfig = new MdDialogConfig();
    dialogConfig.height = '400px';
    let dialogref = this.dialog.open(BrokerSelectDialogComponent);
    dialogref.componentInstance.book = this.book;
  }


  changeBookState(event)
  {
    this.bookService.editBookState(this.book.id,event.value).subscribe((res)=>{
      this.onChangeState.emit(this.book);
    });
  }


  checkUserAuthority(authority:string):boolean
  {
    if(this.cacheService.get('user') != null)
    {
      let user:User = this.cacheService.get('user');
      for (let auth of user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }

}
