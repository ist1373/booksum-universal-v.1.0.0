import {Component, Input, OnInit} from '@angular/core';
import {CategorySection} from '../../entity/model/CategorySection';
import {AppContextUrl, FileContextUrl} from '../../globals';

@Component({
  selector: 'app-category-section',
  templateUrl: './category-section.component.html',
  styleUrls: ['./category-section.component.scss']
})
export class CategorySectionComponent implements OnInit {
  @Input() categorySection:CategorySection ;
  base=AppContextUrl;
  baseFile = FileContextUrl;
  constructor() {
  }




  ngOnInit() {

  }




}
