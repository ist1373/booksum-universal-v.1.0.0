import { Component, OnInit } from '@angular/core';


import {ProductMarket} from '../entity/model/ProductMarket';
import {SliderSection} from '../entity/model/SliderSection';
import {CategorySection} from '../entity/model/CategorySection';
import {SpecialContent} from '../entity/model/SpecialContent';
import {PosterSection} from '../entity/model/PosterSection';
import {BookSection} from '../entity/model/BookSection';
import {BookService} from '../service/book.service';
import {WebComponent, WebComponentType} from "../entity/model/WebComponent";
import {SpecialBook} from "../entity/model/SpecialBook";
import {FileContextUrl} from "../globals";
import {MetaService} from "@ngx-meta/core";



@Component({
  selector: 'app-main-book',
  templateUrl: './main-book.component.html',
  styleUrls: ['./main-book.component.scss'],
  providers: [BookService]
})
export class MainBookComponent implements OnInit {

  WebComponentType:typeof WebComponentType =WebComponentType;
  webComponents :WebComponent[] = [];



  constructor(private bookService:BookService,private readonly metaService: MetaService) {

  }

  ngOnInit() {

    this.metaService.setTitle( 'فروشگاه اینترنتی بوکسام',true);
    this.metaService.setTag( 'og:image','http://file.booksum.ir/logo.jpg');
    this.metaService.setTag('og:description','بزرگترین فروشگاه کتاب های دست دوم');
    this.metaService.setTag('og:locale','fa_IR');
    this.metaService.setTag('og:image:width','200px');
    this.metaService.setTag('og:image:height','200px');

     this.bookService.getBookMarket().subscribe((bookMarket:ProductMarket)=>{
       console.log(bookMarket);
       // if(bookMarket.sliderSection != null)
       // {
         this.loadSlider(bookMarket.sliderSection);
       // }
       this.loadBookSections(bookMarket.bookSections);
       this.loadCategorySection(bookMarket.categorySections);
       this.loadSpecialBook(bookMarket.specialBooks);
       this.loadPosterSection(bookMarket.posterSections);

     })
  }



  loadSlider(sliderSection:SliderSection)
  {

    let webcomp = new WebComponent();
    webcomp.componentType = WebComponentType.SliderSectionWC;
    webcomp.sliderSection = sliderSection;
    this.webComponents[sliderSection.position] = webcomp;
    console.log('has slider');
    console.log(this.webComponents[sliderSection.position]);

  }


  loadBookSections(bookSection:BookSection[]){
    for(let js of bookSection)
    {
      let webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.BookSection;
      webcomp.bookSection = js;
      this.webComponents[js.position] = webcomp;
    }
  }

  loadCategorySection(categorySections:CategorySection[]){
    for(let cs of categorySections)
    {
      let webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.CategorySection;
      webcomp.categorySection = cs;
      this.webComponents[cs.position] =webcomp;
    }
  }

  loadSpecialBook(specialBooks:SpecialBook[]){
    for(let sj of specialBooks)
    {
      let webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.SpecialBook;
      webcomp.specialBook = sj;
      this.webComponents[sj.position] =webcomp;
    }
  }

  loadPosterSection(posterSection:PosterSection[]){
    for(let ps of posterSection)
    {
      let webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.PosterSection;
      webcomp.posterSection = ps;
      this.webComponents[ps.position] =webcomp;
    }
  }

}
