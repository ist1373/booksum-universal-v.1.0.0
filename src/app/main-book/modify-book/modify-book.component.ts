import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UniversityService} from '../../service/university.service';
import {University} from '../../entity/University';
import {CacheService} from 'ng2-cache-service';
import {number} from 'ng2-validation/dist/number';
import {strictEqual} from 'assert';
import {AppContextUrl, FileContextUrl} from '../../globals';
import {ProgressHttp} from 'angular-progress-http';
import {RequestOptions,Headers,Response} from '@angular/http';
import {AccessType, UploadedFile} from '../../entity/UploadedFile';
import {Content, ContentType, ProductState} from '../../entity/Content';
import {CategoryService} from '../../category-dialog/category-service';
import {MdDialog, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {CategoryDialogComponent} from '../../category-dialog/category-dialog.component';
import {plainToClass} from 'class-transformer';
import {Category} from '../../entity/Category';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../entity/User';
import {TagService} from '../../service/tag.service';
import {Tag} from '../../entity/Tag';
import {TagContentType} from '@angular/compiler';
import {BookletService} from '../../service/booklet.service';
import {ActivatedRoute} from '@angular/router';
import {GalleryService} from 'ng-gallery';
import {Book, BookState, InfoType, TagState} from "../../entity/Book";
import {LocationService} from "../../service/location.service";
import {City} from "../../entity/City";
import {BookService} from "../../service/book.service";
// import {CallInfo} from "../../entity/CallInfo";
import {FileService} from "../../service/file.service";
import {Store} from "../../entity/Store";
import {StoreService} from "../../service/store.service";
import {AuthorityName} from "../../entity/security/Authority";
import {SelectComponent} from "ng-select";
import {init} from "protractor/built/launcher";






@Component({
  selector: 'app-modify-book',
  templateUrl: './modify-book.component.html',
  styleUrls: ['./modify-book.component.scss'],
  providers:[UniversityService,CacheService,CategoryService,BookService,TagService,LocationService,FileService,StoreService]
})
export class ModifyBookComponent implements OnInit {

  base = AppContextUrl;
  baseFile = FileContextUrl;
  token = this.cacheService.get('token');
  sampleGalleryImages=[];
  productStates = [];
  modificationType:ModificationType;
  category:Category;
  book:Book;
  universities : Array<any>=[];
  cities:Array<any> = [];
  provinces:Array<any> = [];
  sampleImages:File[]=[];
  coverImage:File;
  coverImageProgresses = new ProgressOfUpload();
  imagesProgresses:ProgressOfUpload[]=[];
  fileAccessType : Array<any>;
  bookStates : Array<any>;
  stores: Array<Store> = [];
  suggestionBooks :Array<Book>;
  clickedForSave = false;

  public ngModelTags = [];// tags to save by tagModule
  public autocompleteBookTags = [];// tags to offer in html

  ModificationType: typeof ModificationType = ModificationType;
  AccessType: typeof AccessType = AccessType;
  InfoType:typeof InfoType = InfoType;
  AuthorityName:typeof AuthorityName = AuthorityName;
  ProductState:typeof ProductState = ProductState;
  BookState:typeof  BookState = BookState;

  bookForm:FormGroup;
  title = new FormControl('', Validators.required);
  author = new FormControl('');
  translator = new FormControl('');
  publisher = new FormControl('');
  pstate = new FormControl('');
  bookstate = new FormControl('');
  univ = new FormControl('');
  prov = new FormControl('');
  bookPrice = new FormControl('', Validators.required);
  sellPrice = new FormControl('', Validators.required);
  publishYear = new FormControl('');
  offPercentage = new FormControl('');
  description = new FormControl('');
  edition = new FormControl('');
  city = new FormControl('');

  callInfoForm:FormGroup;
  email = new FormControl('');
  phone = new FormControl('');
  messengerId = new FormControl('');

  user:User = this.cacheService.get('user');


  constructor(private universityService:UniversityService, private cacheService:CacheService,
              private http: ProgressHttp, private categoryService:CategoryService, public dialog: MdDialog,
              private bookService:BookService, public snackBar: MdSnackBar, private tagService:TagService,
              private activatedRoute:ActivatedRoute,public gallery:GalleryService,private locationService:LocationService,
              private fileService:FileService,private storeService:StoreService) {

    this.bookForm = new FormGroup({
      title:this.title,
      author: this.author,
      translator:this.translator,
      publisher:this.publisher,
      bookPrice:this.bookPrice,
      sellPrice:this.sellPrice,
      publishYear:this.publishYear,
      offPercentage:this.offPercentage,
      description:this.description,
      edition:this.edition,
      pstate:this.pstate,
      bookstate:this.bookstate,
      univ:this.univ,
      prov:this.prov,
      city:this.city,
    });

    this.callInfoForm = new FormGroup({
      email:this.email,
      phone:this.phone,
      messengerId:this.messengerId
    })
  }

  ngOnInit() {
    this.initUniversirsities();
    this.initProvinces();
    this.initBookStates();
    this.activatedRoute.url.subscribe((url)=>{
      if(url[2].toString()=='new')
      {
        if(this.checkUserGrants('ROLE_CONTENT_ADDER'))
        {
          this.email.setValue(this.user.email);
          this.phone.setValue(this.user.phoneNumber);
        }
        this.book = new Book();
        this.initUserGrants();
        this.modificationType = ModificationType.New;
      }
      else
      {
        this.modificationType = ModificationType.Edit;
        this.bookService.getBookByID(Number(url[3])).subscribe((book)=>{
          this.book = book;
          console.log(book);
          this.initUserGrants();
          this.initSampleImages();
          this.title.setValue(book.title) ;
          this.author.setValue(book.author);
          this.translator.setValue(book.translator);
          this.publisher.setValue(book.publisher);
          this.bookPrice.setValue(book.bookPrice);
          this.sellPrice.setValue(book.sellPrice);
          this.publishYear.setValue(book.publishYear);
          this.edition.setValue(book.edition);
          this.pstate.setValue(ProductState[book.productState] + '');
          this.bookstate.setValue(BookState[book.bookState] + '');
          this.category = book.category;

          this.email.setValue(this.book.email);
          this.phone.setValue(this.book.phone);

          if(book.city!=null)
          {
            this.prov.setValue(book.city.province.id+'');
            this.getCitis(book.city.province.id);
            this.city.setValue(book.city.id+'');
          }

          if(book.university!=null)
            this.univ.setValue(book.university.id + '');
          for(let tag of book.tags)
            this.ngModelTags.push({display:tag.tagString,value:tag.id});

        });
      }
    });

    this.fileAccessType = [
      {value: '0', label: 'Public'},
      {value: '1', label: 'Private'},
      {value: '2', label: 'AccessOnPay'},
      {value: '3', label: 'Forbidden'}
    ];

    this.productStates = [
      {value: '0', label: 'در حال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'نیاز به تغییر'},
      {value: '3', label: 'لغو شده'},
      {value: '4', label: 'آرشیو شده'}
    ];

    this.title.valueChanges.subscribe((val)=>{
      if(val.length > 2)
      {
        // this.bookService.findBook(val,null,null,null,null,0,0,0,0,0).subscribe(books=>{
        //   console.log(books);
        //   this.suggestionBooks = books;
        // })
        this.bookService.findSimilarBook(val).subscribe(books=>{
          console.log(books);
          this.suggestionBooks = books;
        })
      }
      else
      {
        this.suggestionBooks = [];
      }
    });
  }
  initBookStates()
  {
      this.bookStates = [
      {value: '0', label: 'تمیز'},
      {value: '1', label: 'مستهلک'},
      {value: '2', label: 'مشابه نو'},
      {value: '3', label: 'نو'}
    ];
  }
  initUserGrants()
  {
    for(let authority of this.user.authorities)
    {
      if(authority.name == 'ROLE_SELLER')
      {
        this.initStores();
        this.book.infoType = InfoType.Shop;
      }
      else {
        this.book.infoType = InfoType.Personal;
      }
    }
  }
  checkUserGrants(name:string):boolean
  {

    for(let authority of this.user.authorities)
    {
      if(authority.name == name) {
        return true;
      }
    }
    return false;
  }

  initStores(){
    this.storeService.getStoreByUser(this.user).subscribe(stores=>{
      this.book.stores = stores;
      for(let st of stores)
      {
        this.stores.push(st);
      }
      console.log(stores);
    });
  }

  selectSuggestion(book:Book)
  {


      this.title.setValue(book.title);
      this.author.setValue(book.author);
      this.publishYear.setValue(book.publishYear);
      this.publisher.setValue(book.publisher);
      this.book.category = book.category;
      this.category = book.category;
      this.description.setValue(book.description);
      this.sellPrice.setValue(book.sellPrice);
      this.bookPrice.setValue(book.bookPrice);
      this.book.coverImage = book.coverImage;
    //   this.fileService.getImageAsFile(book.coverImage.id).subscribe(file=>{
    //     this.coverImage = file;
    // })
  }

  getBookFromLocal(){
    this.bookService.getBookFromLocal().subscribe(book =>{
      this.title.setValue(book.title);
      this.author.setValue(book.author);
      this.publishYear.setValue(book.publishYear);
      this.translator.setValue(book.translator);
      this.publisher.setValue(book.publisher);
      this.sellPrice.setValue(book.sellPrice);
      this.bookPrice.setValue(book.bookPrice);
    });
  }


  initSampleImages()
  {
    for(let image of this.book.images)
    {
      let i={
        src:FileContextUrl+'fid='+image.id,
        thumbnail: FileContextUrl+'fid='+image.id,
        text: image.description
      };
      this.sampleGalleryImages.push(i);
    }
    this.gallery.load(this.sampleGalleryImages);
  }


  openFileBrowser(event,isCover:boolean)
  {
    if(!isCover)
    {
      for(let file of event.target.files)
      {
        this.sampleImages.push(file);
        this.imagesProgresses.push(new ProgressOfUpload());
      }
    }
    else {
      this.coverImage = null;
      this.book.coverImage = null;
      this.coverImageProgresses = new ProgressOfUpload();
      // setTimeout(()=>{
        this.coverImage = event.target.files[0];
      // },200)

    }
  }




  uploadFile(file:File,index:number,isCover:boolean)
  {

    let uploadUrl= AppContextUrl+'file/upload';

    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('accessType', 'Public' );

    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});

    this.http.withUploadProgressListener(progress => {
        if(!isCover)
        {
          this.imagesProgresses[index].progress=progress.percentage;
        }
        else {
          this.coverImageProgresses.progress = progress.percentage;
        }
      })
      .post(uploadUrl,formData,options)
      .subscribe((response) => {

        let uploadedFile = new UploadedFile();
        uploadedFile.id = response.json().code;
        if(!isCover) {
          this.book.images.push(uploadedFile);
          this.imagesProgresses[index].complete = true;
          this.imagesProgresses.splice(index,1);
          this.sampleImages.splice(index,1);
        }
        else {
          this.book.images.push(uploadedFile);
          this.book.coverImage = uploadedFile;
          this.coverImageProgresses.complete = true;
        }
      })
  }



  // deleteImage(i:number){
  //   this.sampleImages.splice(i,1);
  //   this.book.images.splice(i,1);
  //   this.imagesProgresses.splice(i,1);
  // }

  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ='دسته بندی';

    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.category = cat;
      this.book.category = cat
    });
  }

  saveBook()
  {
    if(this.clickedForSave == false)
    {
      this.clickedForSave = true;
      this.book.title = this.title.value;
      this.book.author = this.author.value;
      this.book.publisher = this.publisher.value ;
      this.book.publishYear = this.publishYear.value;
      this.book.bookPrice = this.bookPrice.value;
      this.book.sellPrice = this.sellPrice.value;
      this.book.translator = this.translator.value;
      this.book.description = this.description.value;
      this.book.tagState = TagState.Normal;
      this.book.email = this.email.value;
      this.book.phone = this.phone.value;
      this.book.edition = this.edition.value;
      this.book.messengerId = this.messengerId.value;
      console.log(this.book);
      if(this.book.coverImage!=null)
      {
        this.bookService.saveBook(this.book).subscribe((res)=>{
          console.log(res);
          this.clickedForSave = false;
          if(res.json()['success']==true)
          {
            this.openSnackBar('ذخیره کتاب با موفقیت انجام شد','success');
            this.clearForms();
          }
          else
          {
            this.openSnackBar(res.json()['message'],'error');
          }


        });
      }
      else {
        this.clickedForSave = false;
        this.openSnackBar("لازم است عکس کاور کتاب انتخاب شود",'error');
      }
    }

  }

  editBook(){
    this.book.title = this.title.value;
    this.book.author = this.author.value;
    this.book.publisher = this.publisher.value;
    this.book.publishYear = this.publishYear.value;
    this.book.sellPrice = this.sellPrice.value;
    this.book.bookPrice = this.bookPrice.value;
    this.book.translator = this.translator.value;
    this.book.description = this.description.value;
    this.book.edition = this.edition.value;

    this.book.email = this.email.value;
    this.book.phone = this.phone.value;

    this.bookService.editBook(this.book).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
        this.openSnackBar('ذخیره کتاب با موفقیت انجام شد','success');
      else
        this.openSnackBar(res.json()['message'],'error');
    });
  }

  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }


  public selectStore(item:Store):void {
    console.log('Selected value is: ', item);

    let exist = false;
    let store:Store = item;
    for(let st of this.book.stores)
    {
      if (st.id == store.id)
      {
        exist = true;
      }
    }
    if(!exist)
    this.book.stores.push(item);
  }



  initUniversirsities()
  {
    this.universities = [];
    setTimeout(()=>{
        this.universityService.getUniversities().subscribe((unis:University[])=>{
        this.universities.push({value:'0',label:'انتخاب نشده'});
        for(let uni of unis)
        {
          this.universities.push({value:uni.id+'',label:uni.title.toString()});
        }
      });
    },200);


  }
  public selectUniversity(item:any):void {
    console.log('Selected value is: ', item.value);
    let uni = new University();
    uni.id = item.value;
    this.book.university = uni;
  }


  initProvinces(){
    this.provinces = [];
    setTimeout(()=>{
      this.locationService.getProvinces().subscribe(provs =>{
        for(let prov of provs)
        {
          this.provinces.push({value:prov.id+'',label:prov.title.toString()})
        }
      })
    },200)
  }
  public selectProvince(item:any):void {
    console.log('Selected value is: ', item.value);
    this.cities = [];
    this.getCitis(item.value);
  }

  selectBookState(item:any)
  {
    this.book.bookState = item.value
  }
  selectProductState(item)
  {
    console.log(item.value);
    this.book.productState = item.value;
  }


  getCitis(provinceId:number){

    this.locationService.getCities(provinceId).subscribe(cities =>{
      for(let city of cities)
      {
        this.cities.push({value:city.id+'',label:city.title.toString()})
      }
      // if(this.modificationType == ModificationType.Edit && this.book.city!=null)
      // {
      //   this.city.setValue(this.book.city.id+'');
      // }
    });
  }

  public selectCity(item:any):void {
    console.log('Selected value is: ', item.value);
    let city = new City();
    city.id = item.value;
    this.book.city = city;
  }



  getAutoCompleteTags(tagStr)
  {
    console.log('imannnnnn');
    if(tagStr.length>0)
    {
      this.tagService.getSimilarTages(tagStr).subscribe((tags:Tag[])=>{
        this.autocompleteBookTags = [];
        for (let tag of tags)
        {
          // this.tags.set(tag.tagString,tag.id);
          let atag = {display:tag.tagString,value:tag.id}

          this.autocompleteBookTags.push(atag);
        }
      })
    }
  }

  onRemoveTag(removeTag){
    console.log(removeTag);
    for(let i =0;i<this.book.tags.length;i++ )
    {
      let tag = this.book.tags[i];
      if(tag.tagString == removeTag.display)
      {
        this.book.tags.splice(i,1);

      }
    }
  }

  onAddTag(addedTag)
  {
    console.log(addedTag);
    let tag = new Tag();
    // tag.id = this.tags.get(addedTag.display);
    tag.tagString = addedTag.display;
    tag.id = addedTag.value;
    this.book.tags.push(tag);
  }


  changeImageAcessType(event,i:number)
  {
    this.book.images[i].accessType = this.fileAccessType[event.value].label;
  }



  clearForms(){
    this.title.setValue('');
    this.author.setValue('');
    this.translator.setValue('');
    this.publishYear.setValue('');
    this.ngModelTags = [];
    this.category = null;
    this.publisher.setValue('') ;
    this.bookPrice.setValue('');
    this.sellPrice.setValue('');
    this.edition.setValue('');
    this.sampleImages = [];
    this.imagesProgresses = [];
    this.coverImage= null
    this.coverImageProgresses = new ProgressOfUpload();
    this.book.images = [];
    // this.initBookStates();
    // this.initUniversirsities();
    // this.initProvinces();

    this.cities = [];


    if(this.checkUserGrants("ROLE_SELLER"))
    {
      this.initStores();
    }
    this.initUserGrants();

  }
}

// export enum FileType{Jozve,Image};
export enum ModificationType{Edit,New};

export class ProgressOfUpload{
  progress=0;
  complete=false;
  constructor(){
    this.progress = 0;
    this.complete = false;
  }

}
