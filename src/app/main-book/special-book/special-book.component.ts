import {Component, Input, OnInit} from '@angular/core';
import {AppContextUrl, FileContextUrl} from "../../globals";
import {SpecialBook} from "../../entity/model/SpecialBook";

@Component({
  selector: 'app-special-book',
  templateUrl: './special-book.component.html',
  styleUrls: ['./special-book.component.scss']
})
export class SpecialBookComponent implements OnInit {

  constructor() { }

  base = AppContextUrl;
  baseFile = FileContextUrl;

  @Input() specialBook:SpecialBook;
  ngOnInit() {
  }

}
