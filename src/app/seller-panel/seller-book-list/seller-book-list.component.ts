import {Component, HostListener, OnInit} from '@angular/core';
import {Book} from "../../entity/Book";
import {Store} from "../../entity/Store";
import {StoreService} from "../../service/store.service";
import {BookService} from "../../service/book.service";
import {UserService} from "../../service/user.service";
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";

@Component({
  selector: 'app-seller-book-list',
  templateUrl: './seller-book-list.component.html',
  styleUrls: ['./seller-book-list.component.scss'],
  providers:[StoreService,BookService,CacheService]
})
export class SellerBookListComponent implements OnInit {

  user:User = this.cacheService.get('user');
  books:Book[];
  stores:Store[];
  stores_ngselect : Array<any> = [];
  page = 0;
  store_id = 0;
  scrollOffsetToLoad = 300;


  constructor(private storeService:StoreService,private bookService:BookService,private cacheService:CacheService) { }

  ngOnInit() {
    this.initStores();
  }

  initStores()
  {
      this.storeService.getStoreByUser(this.user).subscribe(stores => {
        this.stores = stores;
        console.log(stores);
        let all = {label:'همه کتاب ها',value:0};
        this.stores_ngselect.push(all)
        for(let store of stores)
        {
          let str = {label:store.title,value:store.id};
          this.stores_ngselect.push(str);
        }
      });
      this.loadUserBooks();
  }

  public selectStore(item:any):void {
    console.log('Selected value is: ', item.value);
    this.page = 0;
    this.store_id = item.value;
    if (this.store_id == 0)
      this.loadUserBooks()
    else
      this.loadBooksByStore(this.store_id,this.page);
  }

  @HostListener('window:scroll', ['$event'])
  doSomething(event) {
    if(this.scrollOffsetToLoad <= window.pageYOffset)
    {
      this.page++;
      if (this.store_id == 0)
      {
        this.loadUserBooks();
      }
      else
      {
        this.loadBooksByStore(this.store_id,this.page);
      }
      this.scrollOffsetToLoad += 700;
    }
  }

  loadBooksByStore(storeId,page)
  {
    this.bookService.getBooksByStore(storeId,page).subscribe(books=>{
      if(this.page == 0)
      {
        this.books = books;
      }
      else
      {
        for(let b of books)
        {
          this.books.push(b);
        }
      }
    });
  }

  loadUserBooks(){
    this.bookService.getUserBooks(this.page).subscribe(books =>{
      if(this.page == 0)
        this.books = books;
      else {
        for(let book of books)
        {
          this.books.push(book);
        }
      }
    });
  }

}
