import { Component, OnInit } from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";

@Component({
  selector: 'app-seller-header',
  templateUrl: './seller-header.component.html',
  styleUrls: ['./seller-header.component.scss']
})
export class SellerHeaderComponent implements OnInit {


  user:User;

  constructor(private cacheService:CacheService) { }

  ngOnInit() {
    this.user = this.cacheService.get('user');
  }


  checkUserAuthority(authority:string)
  {
    if(this.user != null)
    {
      for (let auth of this.user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }
}
