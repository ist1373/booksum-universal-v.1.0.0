import { Component, OnInit } from '@angular/core';
import {StoreService} from "../../service/store.service";
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";
import {Store} from "../../entity/Store";
import {FileContextUrl} from "../../globals";

@Component({
  selector: 'app-seller-store-list',
  templateUrl: './seller-store-list.component.html',
  styleUrls: ['./seller-store-list.component.scss'],
  providers:[StoreService,CacheService]
})
export class SellerStoreListComponent implements OnInit {

  user:User = this.cacheService.get('user');
  stores:Array<Store>;
  base = FileContextUrl;
  baseFile = FileContextUrl;
  lat: number = 32.653000;
  lng: number = 51.668237;
  constructor(private storeService:StoreService,private cacheService:CacheService) { }

  ngOnInit() {
    this.initStores();
  }

  initStores()
  {
    this.storeService.getStoreByUser(this.user).subscribe(stores => {
      this.stores = stores;
    });
  }
}
