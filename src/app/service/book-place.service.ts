import { Injectable } from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {RequestOptions, Headers, Http} from "@angular/http";
import {BookPlace} from "../entity/BookPlace";
import {AppContextUrl, ItemSizePerPage} from "../globals";
import {User} from "../entity/User";
import {plainToClass} from "class-transformer";

@Injectable()
export class BookPlaceService {

  constructor(private cacheService:CacheService,private http:Http) { }

  addBookPlace(bookPlace:BookPlace)
  {
    return this.http.post(AppContextUrl+'bookPlace',bookPlace,this.setAuthorizationHeader());
  }

  getBookPlaceByUser(user:User){
    return this.http.get(AppContextUrl + 'bookPlace/user/' + user.id +'?page='+0+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res)=>{
      var bookPlaces = plainToClass<BookPlace, Object[]>(BookPlace, res.json());
      return bookPlaces;
    })
  }

  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    this.setLocalizationHeader(headers);

    let options = new RequestOptions({ headers: headers});
    return options;
  }
  setLocalizationHeader(headers:Headers)
  {
    if(this.cacheService.exists('activeProvince')) {
      headers.set('province',this.cacheService.get('activeProvince'));
    }
  }
}
