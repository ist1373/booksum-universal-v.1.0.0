import { Injectable } from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {RequestOptions, Headers, Http} from "@angular/http";
import {BookPlace} from "../entity/BookPlace";
import {AppContextUrl, ItemSizePerPage} from "../globals";
import {Store, StoreState} from "../entity/Store";
import {plainToClass} from "class-transformer";
import {User} from "../entity/User";
import {BookRequest, RequestState} from "../entity/BookRequest";
@Injectable()
export class BookRequestService {

  constructor(private cacheService:CacheService,private http:Http) { }


  addBookRequest(bookResuest:BookRequest)
  {
    return this.http.post(AppContextUrl+'bookRequest',bookResuest,this.setAuthorizationHeader());
  }

  getBookRequestsByBookPlace(placeId:number,page:number){
    return this.http.get(AppContextUrl+'bookRequest/place/'+placeId + '?'+'page='+page+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<BookRequest,Object[]>(BookRequest,res.json());
      return books;
    });
  }

  editBookRequestState(id:number,state:RequestState,resText:string){
    return this.http.put(AppContextUrl+'bookRequest/'+id + '?bState=' + state + '&resText='+resText,null,this.setAuthorizationHeader())
  }

  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    this.setLocalizationHeader(headers);

    let options = new RequestOptions({ headers: headers});
    return options;
  }
  setLocalizationHeader(headers:Headers)
  {
    if(this.cacheService.exists('activeProvince')) {
      headers.set('province',this.cacheService.get('activeProvince'));
    }
  }
}
