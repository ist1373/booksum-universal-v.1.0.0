import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {CacheService} from "ng2-cache-service";
import {AppContextUrl, AppContextUrlV2, ItemSizePerPage} from "../globals";
import {plainToClass} from "class-transformer";
import {Book, BookState} from "../entity/Book";
import {ProductMarket} from "../entity/model/ProductMarket";
import {ProductState} from "../entity/Content";

@Injectable()
export class BookService {

  constructor(private http: Http,private cacheService:CacheService) { }


  getBookByID(id:number) {
    return this.http.get(AppContextUrl + 'book/' + id).map(res => {
      let book = plainToClass<Book,Object>(Book,res.json());
      return book;
    });
  }

  getBookByUID(id:string) {
    return this.http.get(AppContextUrlV2 + 'book/' + id,this.setAuthorizationHeader()).map(res => {
      let book = plainToClass<Book,Object>(Book,res.json());
      return book;
    });
  }

  getBooksByURL(url:string) {
    return this.http.get(url,this.setAuthorizationHeader()).map((res) =>{
      let contents= plainToClass<Book,Object[]>(Book,res.json());
      return contents;
    });
  }

  getBooksByState(pState:ProductState,page:number)
  {
     return this.http.get(AppContextUrl + 'book?pState=' + ProductState[pState]+'&page='+page,this.setAuthorizationHeader()).map((res) =>{
       let books= plainToClass<Book,Object[]>(Book,res.json());
       return books;
     })
  }

  getUserBooks(page:number) {
    return this.http.get(AppContextUrl + 'book/myBook?page='+page,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }

  getBooksByCategory(id:number, page:number) {
    return this.http.get(AppContextUrl+'book?catId='+id+'&page='+page,this.setAuthorizationHeader()).map((res) =>{
      let contents= plainToClass<Book,Object[]>(Book,res.json());
      return contents;
    });
  }

  getBooksByTag(id:number, page:number)
  {
    return this.http.get(AppContextUrl+'book/tag/'+id+'?'+'page='+page).map((res) =>{
      let contents= plainToClass<Book,Object[]>(Book,res.json());
      return contents;
    });
  }

  getBooksByStore(id:number, page:number)
  {
    return this.http.get(AppContextUrl+'book/store/'+id+'?'+'page='+page).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }
  getStorableBookByTitle(title:string, page:number){
    return this.http.get(AppContextUrl+'book/storableBook' + '?'+'title='+title+'&'+'page='+page,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }

  getBooksByBookPlace(id:number,page:number)
  {
    return this.http.get(AppContextUrl+'book/bookPlace/'+id+'?'+'page='+page).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }


  getBooksByUser(uid:string,page:number)
  {
    return this.http.get(AppContextUrl+'book/user/'+uid+'?'+'page='+page).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }


  addBookToBookPlace(bookId:number,bookPlaceId:number)
  {
    return this.http.post(AppContextUrl+'book/addToPlace?bookId=' + bookId + '&placeId=' + bookPlaceId,null,this.setAuthorizationHeader());
  }

  deleteBookFromBookPlace(bookId:number,bookPlaceId:number)
  {
    return this.http.post(AppContextUrl+'book/deleteFromPlace?bookId=' + bookId + '&placeId=' + bookPlaceId,null,this.setAuthorizationHeader());
  }

  editBook(book:Book)
  {
    return this.http.put(AppContextUrl+'book',book,this.setAuthorizationHeader());
  }

  editBookState(id:number,state:ProductState){
    return this.http.put(AppContextUrl+'book/'+id+'?pState='+ProductState[state],null,this.setAuthorizationHeader());
  }

  saveBook(book:Book)
  {
    return this.http.post(AppContextUrl+'book',book,this.setAuthorizationHeader());
  }

  getBookMarket() {
    return this.http.get(AppContextUrl + 'market/book',this.setAuthorizationHeader()).map((res)=>{
      var bookMarket = plainToClass<ProductMarket, Object>(ProductMarket, res.json());
      return bookMarket;
    });
  }

  getBookFromLocal() {
    return this.http.get('http://localhost:4600/api/').map((res)=>{
      var book = plainToClass<Book, Object>(Book, res.json());
      return book;
    });
  }



  findBook(title:string,author:string,translator:string,publisher:string,publishYear:string,bookState:BookState,universityId:number,categoryId:number,cityId:number, page:number)
  {
    return this.http.get(AppContextUrl+'book/find?title='+title+'&author='+author+'&translator='+translator+'&publisher='+publisher+
      '&publishYear='+publishYear+ '&bookState='+ (bookState!=null?bookState:'') +'&universityId='+universityId+
      '&categoryId='+categoryId+'&cityId='+(cityId!=null?cityId:'')+'&page='+page,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }

  findSimilarBook(title:string)
  {
    return this.http.get(AppContextUrl+'book/findSimilar?title='+title,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }

  // getCallInfoByBook(id:number){
  //   return this.http.get(AppContextUrl + 'callInfo?bookId=' + id).map((res)=>{
  //     var callInfo = plainToClass<CallInfo, Object>(CallInfo, res.json());
  //     return callInfo;
  //   });
  // }


  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    this.setLocalizationHeader(headers);

    let options = new RequestOptions({ headers: headers});

    return options;
  }

  setLocalizationHeader(headers:Headers)
  {
    if(this.cacheService.exists('activeProvince')) {
      headers.set('province',this.cacheService.get('activeProvince'));
    }
  }

}
