import { Injectable } from '@angular/core';
import {Http, RequestOptions,Headers} from "@angular/http";
import {AppContextUrl, ItemSizePerPage} from "../globals";
import {Comment, CommentState} from "../entity/Comment";
import {classToPlain, plainToClass} from "class-transformer";
import {CacheService} from "ng2-cache-service";

@Injectable()
export class CommentsService {

  constructor(private http:Http,private cacheService:CacheService) { }

  getByState(state:CommentState){
    return this.http.get(AppContextUrl + "comment?cState="+CommentState[state]+'&page='+0+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res)=>{
      var unis= plainToClass<Comment,Object[]>(Comment,res.json());
      return unis;
    });
  }

  editComment(comment:Comment)
  {

    return this.http.put(AppContextUrl + "comment",comment,this.setAuthorizationHeader());
  }

  addComment(comment:Comment)
  {
    return this.http.post(AppContextUrl + "comment",comment,this.setAuthorizationHeader());
  }



  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set("Authorization",this.cacheService.get("token"));
    this.setLocalizationHeader(headers);

    let options = new RequestOptions({ headers: headers});
    return options;
  }
  setLocalizationHeader(headers:Headers)
  {
    if(this.cacheService.exists('activeProvince')) {
      headers.set('province',this.cacheService.get('activeProvince'));
    }
  }
}
