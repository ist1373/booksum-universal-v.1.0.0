import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {CacheService} from "ng2-cache-service";
import {AppContextUrl, ItemSizePerPage} from "../globals";
import {plainToClass} from "class-transformer";
import {Book, BookState} from "../entity/Book";
import {ProductMarket} from "../entity/model/ProductMarket";

import {ContactMessage} from "../entity/ContactMessage";
@Injectable()
export class ContactMessageService {

  constructor(private cacheService:CacheService,private http:Http) { }


  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return options;
  }

  saveContactMessage(contactMessage:ContactMessage)
  {
    return this.http.post(AppContextUrl+'contactMessage',contactMessage,this.setAuthorizationHeader());
  }
}
