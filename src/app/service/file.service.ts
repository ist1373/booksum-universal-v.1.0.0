import { Injectable } from '@angular/core';
import {Http, ResponseContentType, Headers, RequestOptions} from "@angular/http";
import {AppContextUrl, FileContextUrl} from "../globals";
import {SafeUrl, DomSanitizer} from "@angular/platform-browser";
import 'rxjs/add/operator/map'

@Injectable()
export class FileService {

  constructor(private http:Http,private sanitizer:DomSanitizer) { }


  getImageWithId(imageId:number){

    var headers = new Headers();
    headers.append('Content-Type', 'image/jpg');
    let options = new RequestOptions({
      headers: headers,
      responseType: ResponseContentType.Blob
    });
    return this.http.get(AppContextUrl + 'file/'+imageId,options).map(
      (response) => {
        // console.log(response);
        var mediaType = response.headers.get("Content-Type");
        // console.log(mediaType);
        var blob = new Blob([response["_body"]], {type: mediaType});
        var urlCreator = window.URL;
        var url = urlCreator.createObjectURL(blob);
        return this.sanitizer.bypassSecurityTrustUrl( url );
      });
   ;
  }


  getImageAsFile(imageId:number){

    var headers = new Headers();
    headers.append('Content-Type', 'image/jpg');
    let options = new RequestOptions({
      headers: headers,
      responseType: ResponseContentType.Blob
    });
    return this.http.get(FileContextUrl + 'fid='+imageId,options).map(
      (response) => {
        console.log(response);
        var mediaType = response.headers.get("Content-Type");
        // console.log(mediaType);
        let blob = new Blob([response["_body"]], {type: mediaType});
        let arrayOfBlob = new Array<Blob>();
        arrayOfBlob.push(blob);
        let file = new File(arrayOfBlob,'کاور کتاب');
        return file;
      });
    ;
  }




}
