import { Injectable } from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {RequestOptions, Headers, Http} from "@angular/http";
import {BookPlace} from "../entity/BookPlace";
import {AppContextUrl, AppContextUrlV2, ItemSizePerPage} from "../globals";
import {Store, StoreState} from "../entity/Store";
import {plainToClass} from "class-transformer";
import {User} from "../entity/User";

@Injectable()
export class StoreService {

  constructor(private cacheService:CacheService,private http:Http) { }
  addStore(store:Store)
  {
    return this.http.post(AppContextUrl+'store',store,this.setAuthorizationHeader());
  }

  getStoreByUser(user:User){
     return this.http.get(AppContextUrl + 'store/user/' + user.id +'?page='+0+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res)=>{
       var stores = plainToClass<Store, Object[]>(Store, res.json());
       return stores;
     })
  }
  getStoresByCity(cityId:number)
  {
     return this.http.get(AppContextUrl + 'store?provinceId=' + cityId + '&page='+0+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map(res=>{
       var stores = plainToClass<Store, Object[]>(Store, res.json());
       return stores;
     });
  }
  getStoreByID(id:number){
      return this.http.get(AppContextUrl + 'store/' + id).map(res=>{
        var store = plainToClass<Store, Object>(Store, res.json());
        return store;
      })
  }
  getStoreByUID(uid:string){
    return this.http.get(AppContextUrlV2 + 'store/' + uid).map(res=>{
      var store = plainToClass<Store, Object>(Store, res.json());
      return store;
    })
  }


  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    this.setLocalizationHeader(headers);

    let options = new RequestOptions({ headers: headers});
    return options;
  }

  setLocalizationHeader(headers:Headers)
  {
    if(this.cacheService.exists('activeProvince')) {
      headers.set('province',this.cacheService.get('activeProvince'));
    }
  }
}
