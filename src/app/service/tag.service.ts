import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppContextUrl} from "../globals";
import {plainToClass} from "class-transformer";
import {Tag} from "../entity/Tag";

@Injectable()
export class TagService {

  constructor(private http:Http) { }

  getTages()
  {
    return this.http.get(AppContextUrl+'tag').map((res)=>{
      var tags= plainToClass<Tag,Object[]>(Tag,res.json());
      return tags;
    })
  }

  getSimilarTages(str:string)
  {
    return this.http.get(AppContextUrl+'tag?name=' + str +'&page='+0+'&size=' + 100).map((res)=>{
      var tags= plainToClass<Tag,Object[]>(Tag,res.json());
      return tags;
    })
  }
}
