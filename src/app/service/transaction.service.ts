
import { Injectable } from '@angular/core';

import {Http, RequestOptions, Headers} from '@angular/http';
import {AppContextUrl, ItemSizePerPage} from '../globals';
import {Comment, CommentState} from '../entity/Comment';
import {classToPlain, plainToClass} from 'class-transformer';
import {CacheService} from 'ng2-cache-service';
import {User} from "../entity/User";
import {Transaction} from "../entity/Transaction";

@Injectable()
export class TransactionService {

  constructor(private http: Http, private cacheService: CacheService) {

  }


  getUserAsPaidTransactions(page: number){
    return this.http.get(AppContextUrl + 'transaction/user/' + this.getUser().id + '/paid?page=' + page + '&size=' + ItemSizePerPage).map((res) => {
      let transactions = plainToClass<Transaction, Object[]>(Transaction, res.json());
      return transactions;
    });
  }

  getUserAsPayerTransactions(page: number){
    return this.http.get(AppContextUrl + 'transaction/user/' + this.getUser().id + '/payer?page=' + page + '&size=' + ItemSizePerPage).map((res) => {
      let transactions = plainToClass<Transaction, Object[]>(Transaction, res.json());
      return transactions;
    });
  }

  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization', this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return options;
  }

  getUser(){
    let user: User;
    return user = this.cacheService.get('user')
  }
}
