import { Injectable } from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {Http} from "@angular/http";
import {AppContextUrl} from "../globals";
import {plainToClass} from "class-transformer";
import {University} from "../entity/University";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UniversityService {

  constructor(private http:Http,private cacheService: CacheService) {

  }
  getUniversities()
  {
      return this.http.get(AppContextUrl+"university").map((res)=>{
        var unis= plainToClass<University,Object[]>(University,res.json());

        return unis;
      })
  }

}
