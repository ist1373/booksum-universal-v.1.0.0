import { Injectable } from '@angular/core';
import {AppContextUrl} from "../globals";
import {plainToClass} from "class-transformer";
import {Headers, Http,RequestOptions} from "@angular/http";
import {JwtAuthenticationRequest} from "../entity/security/JwtAuthenticationRequest";
import {User} from "../entity/User";
import {Subject} from "rxjs/Subject";
import {CacheService} from "ng2-cache-service";
import {Student} from "../entity/Student";



@Injectable()
export class UserService {

  constructor(private http:Http,private cacheService: CacheService) {

  }

  public static tokenHeader = "Authorization";
  public userChange: Subject<User> = new Subject<User>();

  login(email:string,password:string) {
    var body= new JwtAuthenticationRequest();
    body.username = email;
    body.password = password;
    return this.http.post(AppContextUrl + 'user/auth',body);
  }

  getUser()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return this.http.get(AppContextUrl + 'user',options);
  }
  registerationConfirm(token:string)
  {
    return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  }

  getUsersByUserName(username)
  {
    return this.http.get(AppContextUrl + 'user?username=' + username ,this.setAuthorizationHeader()).map(res =>{
      var bookMarket = plainToClass<User, Object[]>(User, res.json());
      return bookMarket;
    });
  }

  registerFinalUser(user:Student)
  {
    return this.http.post(AppContextUrl + 'user/student',user);
  }


  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return options;
  }

  editUser(user:User)
  {
    return this.http.put(AppContextUrl+'user/student',user,this.setAuthorizationHeader());
  }
}
