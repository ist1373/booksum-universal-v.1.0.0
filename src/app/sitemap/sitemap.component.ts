import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss']
})
export class SitemapComponent implements OnInit {
  xml : any;
  constructor() {

    this.xml ='<?xml version="1.0" encoding="UTF-8"?>'+
      '<urlset'+
      ' xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'+
      ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'+
      ' xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9'+
      'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'+
      '<!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->'+
      '  <url>'+
      '    <loc>http://booksum.ir/</loc>'+
      '<lastmod>2017-06-29T09:04:30+00:00</lastmod>'+
      '<changefreq>daily</changefreq>'+
      '</url>'+
      '<url>'+
      '<loc>http://booksum.ir/content</loc>'+
      ' <lastmod>2017-06-29T09:04:30+00:00</lastmod>'+
      '<changefreq>daily</changefreq>'+
      '</url>'+
      '</urlset>';

  }




  ngOnInit() {
  }

}
