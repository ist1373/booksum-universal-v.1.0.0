import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {SliderSection} from "../entity/model/SliderSection";
import {Slider} from "../entity/Slider";
import {FileService} from "../service/file.service";
import {Response, Http, ResponseContentType, RequestOptions , Headers} from "@angular/http";

import {getImportClauses} from "@angular/cli/utilities/get-dependent-files";

import {AppContextUrl, FileContextUrl} from "../globals";
import {DomSanitizer} from "@angular/platform-browser";
import {SafeUrl} from "@angular/platform-browser/public_api";

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  providers:[SliderSection,FileService]
})
export class SliderComponent implements OnInit {

  @Input()
  sliderSection:SliderSection;
  base=AppContextUrl;
  baseFile = FileContextUrl;
  imgurl:SafeUrl="";

  // @ViewChild('photo') img;

  // sliderPages:Array<SliderPage>;

  constructor(private fileService:FileService,private http:Http,private sanitizer:DomSanitizer) {
     // this.sliderPages = new Array<SliderPage>();

  }

  ngOnInit() {
    console.log(this.sliderSection)
    console.log('imaaaaaaaaaaaaaaaaaaaan');

  }

  beautifyTitle(title:string)
  {
    return title.replace(/ /g,'-');
  }
  // initSlider(slider:SliderSection)
  // {
  //
  //   for (let i = 0;i<slider.sliderList.length;i+=3)
  //   {
  //       let sliderPage= new SliderPage();
  //       for(let j = i;j<i+3;j++)
  //       {
  //         sliderPage.sliders.push(slider.sliderList[j]);
  //         // this.getImage(slider.sliderList[j].image.id,slider.sliderList[j]);
  //       }
  //       this.sliderPages.push(sliderPage);
  //   }
  // }

  getImage(id:number,slider:Slider)
  {
    this.fileService.getImageWithId(id).subscribe((safeurl)=>{
        slider.imageUrl = safeurl;
    })

    // var headers = new Headers();
    // headers.append('Content-Type', 'image/jpg');
    // let options = new RequestOptions({
    //   headers: headers,
    //   responseType: ResponseContentType.Blob
    // });
    // this.http.get(AppContextUrl + 'file/'+3,options).subscribe(
    //   (response) => {
    //     console.log(response);
    //     var mediaType = response.headers.get("Content-Type");
    //     // console.log(mediaType);
    //      var blob = new Blob([response["_body"]], {type: mediaType});
    //      var urlCreator = window.URL;
    //      var url = urlCreator.createObjectURL(blob);
    //      this.imgurl = this.sanitizer.bypassSecurityTrustUrl( url );
    //   });

  }



}


