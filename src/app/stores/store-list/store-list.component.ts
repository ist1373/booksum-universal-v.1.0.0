import { Component, OnInit } from '@angular/core';
import {StoreService} from "../../service/store.service";
import {Store} from "../../entity/Store";
import {CacheService} from "ng2-cache-service";
import {number} from "ng2-validation/dist/number";

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss'],
  providers:[StoreService,CacheService]
})
export class StoreListComponent implements OnInit {

  stores:Store[];
  constructor(private storeService:StoreService,private cacheService:CacheService) { }

  ngOnInit() {
       this.loadStoresByCity();
  }

  loadStoresByCity()
  {
    console.log(this.cacheService.get('activeProvince'))
    let storeCity = this.cacheService.get('activeProvince');
    this.storeService.getStoresByCity(storeCity).subscribe(res=>{
       console.log(res);
       this.stores = res;
    });
  }

}
