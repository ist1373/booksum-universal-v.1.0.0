import {Component, HostListener, OnInit} from '@angular/core';
import {Book} from "../../entity/Book";
import {Store} from "../../entity/Store";
import {ActivatedRoute, Router} from "@angular/router";
import {StoreService} from "../../service/store.service";
import {BookService} from "../../service/book.service";
import {MetaService} from "@ngx-meta/core";

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.scss'],
  providers : [StoreService,BookService]
})
export class StorePageComponent implements OnInit {
  store = new Store();
  books:Book[] = [];
  page = 0;
  moreElement;
  noLoadMore=false;

  constructor(private activatedRoute:ActivatedRoute,private storeService:StoreService,private bookService:BookService,
              private readonly metaService: MetaService) { }

  ngOnInit() {
    this.loadStoreDetails();
    this.moreElement = <HTMLElement>document.getElementById('more');
  }

  loadStoreDetails()
  {
      this.activatedRoute.url.subscribe((url)=>{
        if(url[0].toString()=="toranj")
        {
          this.loadStoreByUid('b9d06396-9461-11e7-9f02-00163ed8440f')
        }
      });

     this.activatedRoute.params.subscribe(params =>{
        console.log(params['id'])
        this.loadStoreByUid(params['id'])
     });
  }



  loadStoreByUid(uid){
    this.storeService.getStoreByUID(uid).subscribe(store =>{
      this.store = store;
      this.loadBooksOfStore();
      this.setMetaServices();

    })
  }

  loadBooksOfStore()
  {
      this.bookService.getBooksByStore(this.store.id,this.page).subscribe(books =>{
        if(this.page == 0)
        {
          this.books = books;
        }
        else
        {
          for(let book of books)
          {
             this.books.push(book);
          }
        }
      });
  }

  setMetaServices()
  {
    this.metaService.setTitle(' فروشگاه '+this.store.title.toString() ,true);
    this.metaService.setTag('og:description',this.store.address.toString() + ' '+ this.store.phoneNumber!=null?this.store.phoneNumber.toString():'');
    this.metaService.setTag('og:locale','fa_IR');
  }


  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    if(this.checkVisible(this.moreElement))
    {
        if(!this.noLoadMore)
        {
          this.page++;
          this.loadBooksOfStore();
          this.noLoadMore = true;
          console.log('load more!')
        }

    }
    else
    {
       this.noLoadMore = false;
    }
  }
  checkVisible(el) {
    var elemTop = el.getBoundingClientRect().top;
    var elemBottom = el.getBoundingClientRect().bottom;
    var isVisible = (elemBottom <= window.innerHeight);
    return isVisible;
  }

}
