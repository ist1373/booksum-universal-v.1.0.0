import {Component, HostListener, OnInit} from '@angular/core';
import {Book} from "../../entity/Book";
import {BookService} from "../../service/book.service";

@Component({
  selector: 'app-user-book-list',
  templateUrl: './user-book-list.component.html',
  styleUrls: ['./user-book-list.component.scss'],
  providers:[BookService]
})
export class UserBookListComponent implements OnInit {
  books:Book[] = [];
  page :number;
  scrollOffsetToLoad = 300;
  constructor(private bookService:BookService) { }

  ngOnInit() {
    this.page = 0;
    this.loadUserBooks();
  }

  loadUserBooks(){
    this.bookService.getUserBooks(this.page).subscribe(books =>{
      if(this.page == 0)
      this.books = books;
      else {
        for(let book of books)
        {
          this.books.push(book);
        }
      }
    });
  }

  @HostListener('window:scroll', ['$event'])
  loadMore(event) {
    // console.log(window.pageYOffset);
    if(this.scrollOffsetToLoad <= window.pageYOffset)
    {
      console.log(this.page);
      this.page = this.page +1 ;
      this.loadUserBooks();
      this.scrollOffsetToLoad += 700;
    }
  }
}
