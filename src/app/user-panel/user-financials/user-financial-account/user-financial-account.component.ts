import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-user-financial-account',
  templateUrl: './user-financial-account.component.html',
  styleUrls: ['./user-financial-account.component.scss']
})
export class UserFinancialAccountComponent implements OnInit {

  financialForm: FormGroup;
  national = new FormControl('');
  postal = new FormControl('');
  shaba = new FormControl('');

  constructor() {
    this.financialForm = new FormGroup({
      national: this.national,
      postal: this.postal,
      shaba: this.shaba
    });
  }



  ngOnInit() {
  }


}
