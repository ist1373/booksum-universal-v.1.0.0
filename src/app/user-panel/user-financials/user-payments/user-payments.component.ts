import { Component, OnInit } from '@angular/core';
import {TransactionService} from "../../../service/transaction.service";
declare var Jalaali:any;

@Component({
  selector: 'app-user-payments',
  templateUrl: './user-payments.component.html',
  styleUrls: ['./user-payments.component.scss'],
  providers: [TransactionService]
})
export class UserPaymentsComponent implements OnInit {


  transactions = [];
  constructor(private transactionService : TransactionService) { }

  ngOnInit() {


    this.transactionService.getUserAsPayerTransactions(0).subscribe((trans) => {
      console.log(trans);
      this.transactions = trans;
    });
  }

  getJalaliDate(date: Date){
    Jalaali.toJalaali(date.getFullYear(), date.getMonth(), date.getDay());
  }

}
