import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decodeUrlPipe'
})
export class DecodeUrlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let newValue = value.replace(/ /g, '-');
    return newValue;
  }

}
