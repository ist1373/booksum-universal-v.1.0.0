import 'reflect-metadata';
import 'zone.js/dist/zone-node';
import { renderModuleFactory } from '@angular/platform-server'
import { enableProdMode } from '@angular/core'
import { AppServerModuleNgFactory } from '../dist/ngfactory/src/app/app.server.module.ngfactory'
import * as express from 'express';
import { readFileSync } from 'fs';
import { join } from 'path';

var compression = require('compression');

var Client = require('node-rest-client').Client;




var options = {
  mimetypes: {
    xml: ["application/xml"]
  }
};

var client = new Client(options);


const PORT = process.env.PORT || 4400;

enableProdMode();

const app = express();

app.use(compression());



let template = readFileSync(join(__dirname, '..', 'dist', 'index.html')).toString();

app.engine('html', (_, options, callback) => {
  const opts = { document: template, url: options.req.url };

  renderModuleFactory(AppServerModuleNgFactory, opts)
    .then(html => callback(null, html));
});

app.set('view engine', 'html');
app.set('views', 'src')

app.route('/sitemap.xml')
  .get((req, res) => {
    client.get("http://api.booksum.ir/api/v1/sitemap/booksum.xml", function (data, response) {
      console.log(data);
      console.log(response);

      res.header('Content-Type','text/xml').send(data);
    });
  });

app.get('*.*', express.static(join(__dirname, '..', 'dist')));

app.get('*', (req, res) => {
  res.render('index', { req });
});





var server = app.listen(PORT, () => {
  console.log(`listening on http://localhost:${PORT}!`);
});

server.on('connection', function(socket) {
  console.log("A new connection was made by a client.");
  socket.setTimeout(30 * 1000);
  // 30 second timeout. Change this as you see fit.
})
