const path = require('path');
const nodeExternals = require('webpack-node-externals');
// /^@agm\/core/,
// compodoc -s

// /^jalali-moment/,
// /^ng-lazyload-image/,
module.exports = {
  entry: {

    server: './src/server.ts',
    polyfills: './src/polyfills.ts'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  target: 'node',
  externals: [nodeExternals({
    whitelist: [
      /^@angular\/material/,
      /^@angular\/cdk/,
      /^@agm\/core/,
      /^angular-bootstrap-md/,
      /^ng-gallery/,
      /^ng2-slim-loading-bar/,
      /^mdbootstrap/,
      /^ng2-validation/,
      /^angular2-tag-input/,
      /^angular-progress-http/,
      /^angular-star-rating/,
      /^ng2-tag-input/,
      /^ng-select/,
      /^ng2-select/,
      /^@ngx-meta/,
      /^ngx-chips/,
      /^ng2-material-dropdown/,
      /^ng2-device-detector/,
      /^owl.carousel/,
      /^ngx-owl-carousel/,
      /^ng2-device-detector/,
    ]
  })],
  node: {
    __dirname: true
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.ts$/,
        loader: 'ts-loader'
    }
    ]
  }
}
  // /^angular-star-rating/,
